# Neuroscience

Respository of useful tools and instructions used and/or created by Alina Goncharova while working on neuroscience studies at the Developmental Cognition & Neuroimaging Lab.

- [analysis](analysis/)
- [animals](animals/)
- [image_processing](image_processing/)
- [mri](mri/)
- 


---

## Projects

---

1. Rat IL6 Behavior + MRI
2. Rat Opto-fMRI
3. Mouse Opto-fMRI
4. Mouse microPET meth Study
5. Rat IL6 Volume Analysis Script Development
6. Immunofluorescence Protocol Development
7. Rat IL6 Volume Analysis
8. Rat IL6 Immunofluorescence Staining

---

---

### 1. Rat IL6 Behavior + MRI | Prenatal Exposure to Interleukin-6 in Rats


```mermaid
graph LR;
1(Rat IL6) --- A(MRI)
style 1 stroke-width:4px
1 --- B(Behavior)
1 --- C(Samples)

A --- ai(Structural T2)
A --- aii(Functional EPI)

B --- bi(Light Dark Box)   
B --- bii(Locomotor)   
B --- biii(Social Interaction)   
B --- biv(Fear Conditioning)   

C --- ci(Brain)
C --- cii(Blood Plasma)
C --- ciii(Feces)
C --- civ(Frozen Intestines)

ci --- ci1(Frozen Sections)
   ci1 --- ci1a(PFC)
   ci1 --- ci1a(PFC)
   ci1 --- ci1a(PFC)
   ci1 --- ci1a(PFC)
   ci1 --- ci1a(PFC)
ci --- ci2(Whole Brain)
   ci2 --- ci2a(Cyrostat -> slides)
   ci2 --- ci2b(Paraffin -> slides)

cii --- cii1(Cardiac Puncture)
cii --- cii2(Tail Vein)

ciii --- ciii1(Collected from cage)
ciii --- ciii2(Perfusion Colon Content)

civ --- civ1(Ilium)
civ --- civ2(Colon)

click A "https://iacucapp.ohsu.edu" "eIACUCII"
    style A fill:#AED6F1
    click B "https://eibc.ohsu.edu/IBC/" "eIBC"
    style B fill:#AED6F1
```    
    
    
    
Poster (Flux)
![il6_flux.png](screenshots/il6_flux.png)

Data
![ratil6.png](screenshots/ratil6.png)

### 2. Rat Opto-fMRI | Opto-fMRI of VTA dopamine neuron sctivation in TH-Cre Rats


### 3. Mouse Opto-fMRI | Opto-fMRI + Calcium Imaging of DRN serotonin neuron activation in SERT-Cre Mice


### 4. Mouse microPET meth Study | MicroPET + CT + MRI of Methamphetamine stimulated dopamine relsease in MAHDR & MALDR Mice


### 5. Volume Analysis Script Development 


### 6. Immunofluorescence Protocol | Development of Immunofluorescence Protocol for Whole Brain GFAP, TH, VGLUT2, DAPI Staining in Rat Brain Tissue


### 7. Rat IL6 Volume Analysis | Volume Analysis of Prenatal Exposure to Interleukin-6 in Rats


### 8. Rat IL6 Immunofluorescence Staining | Immunofluorescence Staining of Whole Brain GFAP, TH, VGLUT2, DAPI Staining in Rat Brain Tissue


---

## Operations

---

```mermaid
graph LR;
    1(Regulated Protocols) --- A(IACUC)
    style 1 stroke-width:4px
    1 --- B(IBC)
    click A "https://iacucapp.ohsu.edu" "eIACUCII"
    style A fill:#AED6F1
    click B "https://eibc.ohsu.edu/IBC/" "eIBC"
    style B fill:#AED6F1
A --- ai(Laboratory animals)
B --- bi(Recombinant or synthetic DNA and infectious agents)

2(Documentation) --- C(Box)
 style 2 stroke-width:4px
2 --- D(GitLab)
2 --- E(Trello)
2 --- F(Wiki)
2 --- G(Xdrive)
    click C "https://ohsu.box.com/s/gp10zr5namu1c0mpsqv8673lq8dg6zsu" "box"
style C fill:#AED6F1
    click D "https://gitlab.com/Fair_lab/RODENT" "gitlab"
    click E "https://trello.com/b/hr3GEsmP/optogenetics-fmri" "Trello"
    style D fill:#AED6F1
click E "https://trello.com/b/hr3GEsmP/optogenetics-fmri" "Trello"
style E fill:#AED6F1
   click F "https://aircwiki.ohsu.edu/DCANWiki" "wiki"
style F fill:#AED6F1
C --- ci(Project and operations files)
D --- di(Version controlled code and how-to guides)
E --- ei(To track team progress and update PIs)
F --- fi(AIRC and DCAN Lab info)
G --- gi(DCAN Lab files)

4(Scheduling) --- J(iLab)
 style 4 stroke-width:4px
4 --- K(outlook)
click J "https://ohsu.corefacilities.org/account/login" "iLab"
style J fill:#AED6F1
J --- ji(OHSU cores, AIRC, ALMC)
K --- ki(team meetings)

5(Ordering) --- M(DCM-OPS)
 style 5 stroke-width:4px
5 --- N(BNorders)
5 --- O(OHSU Logistics)
5 --- P(OHSU Pharmacy)
5 --- Q(Research Store)
5 --- R(Pcard)
    click M "https://dcmops.ohsu.edu/" "DCM-OPS"
    style M fill:#AED6F1
click O "https://o2.ohsu.edu/logistics/purchasing-portal/index.cfm" "logistics"
    style O fill:#AED6F1
M --- mi(Animal orders and services via online portal)
N --- ni(email PO request form to dept)
O --- oi(supplies via logistics online portal)
P --- pi(fax pharmacy order sheet to pharmacy)
Q --- qi(Buy from store Mackenzie Hall rm 1197)
R --- ri(email supplies links to lab admin)

3(Samples, Supplies, and Equipment Storage) --- S(Frozen Samples)
style 3 stroke-width:4px
3 --- T(Refrigerated Samples and supplies)
3 --- W(Frozen supplies)
3 --- U(Misc Supplies)
3 --- V(MRI related equipment and supplies) 
S --- si(OCTRI -80*C Freezers)
T --- ti(AIRC LBRB rm 154 4*C fridge)
U --- ui(DCAN Lab cabinets)
V --- vi(12T MRI suite cabinets)
W --- wi(AIRC rm 154 -20*C freezer)
```

- A. Image Processing
- B. MRI Acquisition
- C. Analysis
- D. Coding/Scripting
- E. Regulatory Compliance
- F. IACUC Protocols
- G. Animal Husbandry
- H. Sample Collection/Storage
- I. Ordering

---

### A. Image Processing

Up to date image processing information can be found in the gitlab [RODENT repository](https://gitlab.com/Fair_lab/RODENT?nav_source=navbar). Processing used by Alina included [here](image_processing/).

### B. MRI Acquisition

Scan acquisition is project specific. Parameters must be optimized for each study and species. General 12T use will be taught by someone at the AIRC. 

### C. Analysis

Analyses are project specific. Please review project documentation on the gitlab [repository](https://gitlab.com/Fair_lab/RODENT?nav_source=navbar).

### D. Coding/Scripting

Information can be found via the [aircwiki](https://aircwiki.ohsu.edu/Home) (Note: must be on secure network to view wiki), [exacloud documentation](https://www.ohsu.edu/advanced-computing-center/acc-and-exacloud-cluster), or the [fair_lab gitlab](https://gitlab.com/Fair_lab?nav_source=navbar).

### E. Regulatory Compliance

As the owner of rodent team projects, it is your responsibility to ensure that all team members are in compliance. Use Cognos to run a Researcher Snapshot Report. Review compliance and which protocols staff are assigned to. Staff may not directly work with animals unless they are listed on that specific protocol.

Example Researcher Snapshot report:
![](https://i.imgur.com/fpGXe2l.png)


### F. IACUC Protocols

Update IACUC protocols anytime new staff, methods, techniques, or procedures will be used on live animals. Procedures must be approved by IACUC before use.

Each IACUC approved project (i.e. protocol) may have multiple studies and animal groups within it.

eIACUCII Approved Projects View:
![](https://i.imgur.com/rplW7Km.png)


### G. Animal Husbandry

Manage animal husbadry needs via [DCM OPS](https://dcmops.ohsu.edu/) or by contacting DCM directly.

DCM OPS Protocol View:
![](https://i.imgur.com/pgyIBZO.png)


### H. Sample Collection/Storage

Any samples that need to be stored at -80 degrees C are stored at the Oregon Clinical & Translational Research Institute [OCTRI](https://www.ohsu.edu/octri). 

### I. Ordering

Order supplies via OHSU Pharmacy, Pcard, logistics, or Behavioral Neuroscience Department PO.

### J. Equipment & Supplies 

Team supplies are currently stored in the AIRC 12T room, AIRC chem lab chemical storage cabinet, and DCAN Lab cabinets.


K. 
L.
















![brainhands.jpeg](screenshots/brainhands.jpeg)