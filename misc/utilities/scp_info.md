# SCP

Secure copy protocol (SCP) securely transfers files via Secure Shell (SSH) between a local (or remote) and remote host.

## Download

Download an SCP client for your OS
-  SCP client for Windows: https://winscp.net/eng/index.php 

### WinSCP

1. New Session 
- File protocol: SCP
- Host name: airclogin-a1
- Port number: 22
- User name
- Password
- Click Login

2. Navigate to correct local and remote directories
- Select files
- Click `Files` and select `Upload`



### Links

- [Winscp](https://winscp.net/eng/index.php)
- [Copying files from Linux to MAC](https://research.csc.fi/csc-guide-copying-files-from-linux-and-mac-osx-machines-with-scp)


