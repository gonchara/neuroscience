Transfers
===

#### Table of contents

1. [Imports](#section-1)
   
2. [Internal Transfers](#section-2)
 


---

## 1. [Imports](#section-1)

Transfers from other institutions will be coordinated between both study teams, vets, and animal departments. All animals imported from other institutions will need to go through pathogen testing at the sending instituion and quarantine, for 3 weeks, at OHSU. Expedited quarantine can be requested with a scientific justification.

- Coordinate with outside institution (i.e Unapproved Vendor)
    -  Get Info: Strain/Stock/Breed, Genotype, Sex, Quantity, Vet contact info, date for transfer 
-  Create import request via [DCM-OPS](https://dcmops.ohsu.edu/)
    - Select IACUC tab
    - Select the protocol that animals will be imported onto.
    - select Create Order/Transfer (under Create SR along the left of the screen)
    - Select Animal Order or Import (under the Getting Started page)
    - Provide the receiving contact name for import (name and number will go on cage card)
    - Select the animal group to import animals onto.
    - Select the Add button to add a line item (under Order Line items)
    - Add a new import line item if there is any variation in: animal group, strain, genotype, weight, age, sex, arrival dates, per diem alias
    - Select Unapproved Vendor
    - Document Sending Institution name, address, Material Transfer Agrement #, and contact info for primary, invesigator, shipping, and veterinary contacts
    - In Additional Comments, include a scientific justification for Expedited Quarantine if applicable. 
    - Upon completion of the import request, a reminder banner will appear on the submission
screen as a reminder that `This request have not been submitted yet`. 
    - Select Submit (along the left of the screen).
    - You can edit the order request prior to submission; however, the request is not
editable after submission, unless DCM requests Clarification of Information.
    - Once an order is initiated, cancellation of the request requires DCM approval.

Example Scientific Justificatin:
"Scientific Justification for Expedited Quarantine: 
Animals will be injected with an adeno-associated virus (AAV) encoding the optogenetic protein Chrimson. Expression of the protein typically takes two weeks to reach levels that allow for optogenetic manipulation of neural activity. Four to six weeks post-injection, some viruses cause levels of expression that can lead to toxicity and cell death. Therefore the ideal experimental window is between two and six weeks post viral injection. Given that the animals will be monitored for at least three days after surgery at UO, they will likely arrive at OHSU approximately one week after viral injection. This means that animals should start to be imaged within one week of arriving at OHSU.""

## 2. [Internal Transfers](#section-2)

Instructions on [DCM website](https://o2.ohsu.edu/comparative-medicine/training-handling/index.cfm)














































