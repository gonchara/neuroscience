# Image Processing

- [opto_fmri_proc.md](opto_fmri_proc.md/) fMRI image processing via DCAN lab pipeline for optogenetics-fMRI studies
- [spm_processing.md](spm_processing.md) fMRI image processing & analysis via SPM code from Pittsburgh