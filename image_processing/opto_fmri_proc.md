# Optogenetrics-fMRI Rodent Processing 

Optogenetics-fMRI rodent image preprocessing occurs on the exahead1.ohsu.edu login server with computing support on the [Slurm Cluster](https://aircwiki.ohsu.edu/SlurmCluster) (<-requires airc access).

Required scripts, pipelines, utilities, images etc.:
- [opto-fMRI_pipeline.sh](scripts/opto-fMRI_pipeline.sh)
- [run_rodent_subj.sh](scripts/run_rodent_subj.sh)
- [MASK_ANTS_T2_d_bc.sh](scripts/MASK_ANTS_T2_d_bc.sh)
- [MASK_ANTS_TOPUP.sh](scripts/MASK_ANTS_TOPUP.sh)
- [motion_correction.sh](scripts/motion_correction.sh)
- [bruker2nifti](https://github.com/SebastianoF/bruker2nifti/blob/master/README.md)


## I. Copy Data, Prep, & Edit Mask

MRI scans acquired with the 12T are backed up from the 12T computer to AIRC each midnight after the scan. 

### Copy Data

If copying Bruker data before it's been backed up:
- Log on to target server
- Pull data with `scp` command

Example command:
```
scp -r mpike@airc12T:/opt/PV5.1/data/mpike/nmr/PIKE01172019_DexIsoMouse.Ro1 /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/input/
```

If copying Bruker data after it's been backed up:
- Logon to either backed up location or target location
- Copy data with `rsync` command

Forward Scans 
Example command (from @airclogin-a1):
```
rsync -urlHSE /bruker/12T/mpike/nmr/PIKE03292019_MOUSE_ELEC_STIM.Sx1 gonchara@exacloud:/home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/input/
```

Reverse Scans
Example Command (from @airclogin-a1):
```
rsync -urlHSE /bruker/12T/mpike/nmr/PIKE03292019_MOUSE_ELEC_STIM_REVERSE.Sx1 gonchara@exacloud:/home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/input/

```


### Prep Data

Data is prepared for processing with the `opto-fMRI_pipeline.sh` script, which copies the original data to an output folder, converts to niftis (with [Bruker2nifti](https://github.com/SebastianoF/bruker2nifti/wiki/Graphical-User-Interface-Examples)), renames the scans, denoises, biasfield corrects and masks the T2.

- Edit command for `opto-fMRI_pipeline.sh` with options `-b` `-t` `-e` `-r` `-n` to pull and mask data. Use the scan notes to determine which scans the scan numbers represent.

>
    - Subject ID (`-s`)
    - Raw bruker data path (`-b`)
    - T2 scan number (`-t`)
    - EPI scan numbers (`-e`)
    - REV_EPI  bruker data path (`-r`)
    - REV_EPI scan number (`-n`)
    - Desired or existing output directory (`-o`)

- Submit command 

Example command:
```
/home/exacloud/tempwork/fnl_lab/code/internal/pipelines/opto-fMRI/opto-fMRI_pipeline.sh -o /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/output/PIKE02112019_RAT4303_ELEC_STIM/20190211 -s PIKE02112019_RAT4303_ELEC_STIM -b /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/input/PIKE02112019_RAT4303_ELEC_STIM.RN1 -r /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/input/PIKE02112019_RAT4303_ELEC_STIM_REVERSE.RN1 -n 5 -t 10 -e "12 13 14 15 16 17 18 19 20 21 22 23"
```

- Output: b2n_data, b2n_rev_data, FWD_EPIs, REV_EPI, T2_late2epi, T2_late2epi_d, T2_late2epi_d_bc, T2_mask_RBET_mask

### Edit T2 Mask

Automatic masking creates a binary map marking portions of the image that contain the brain. These generated masks require some manual editing which is accomplished in FSLView.

- Navigate to subject folder (Example: `cd /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/output/PIKE02112019_RAT4303_ELEC_STIM/20190211/`)
- Open T2 & mask with FSLView (Example: `fslview_deprecated T2_late2epi_d_bc.nii.gz T2_mask_RBET_mask.nii.gz)
- Edit T2 mask
	- In the bottom right, unclick the lock on the mask file, click blue "i" icon, change "lookup table options" to Red. 
	- When done save as: `T2_mask_RBET_mask_EDIT` (Output: T2_mask_RBET_mask_EDIT.hdr & T2_mask_RBET_mask_EDIT.img)

### Edit Topup Mask

- Navigate to subject folder (Example: `cd /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/output/PIKE02112019_RAT4303_ELEC_STIM/20190211/`) 
- Open topup image & mask in FSLView (Example: fslview_deprecated top_up_mask.nii.gz opto-fMRI_pipeline/topup_applied.nii.gz) 
- Edit topup mask
	- In the bottom right, unclick the lock on the mask file, click blue "i" icon, change "lookup table options" to Red. 
	- When done save as: `top_up_mask_EDIT` (Output: top_up_mask_EDIT.hdr & top_up_mask_EDIT.img)
	- If output is `top_up_mask_EDIT.nii.gz` instead, convert to analyze format (Example: fslchfiletype ANALYZE top_up_mask_EDIT.nii.gz)

## II. Run thru Topup 

Topup runs the first portion of the pipeline. * do -u and -p do the same thing now?*

- Edit command for `opto-fMRI_pipeline.sh` with option `-u` to run topup
    
Example command:
```
/home/exacloud/tempwork/fnl_lab/code/internal/pipelines/opto-fMRI/opto-fMRI_pipeline.sh -o /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/output/PIKE02112019_RAT4303_ELEC_STIM/20190211 -s PIKE02112019_RAT4303_ELEC_STIM -u
```

- Output: `EPI_scout_to_T2.nii.gz` `T2_to_ATLAS.nii.gz` `FWD_EPI#_final.nii.gz`
- *Once job is complete check the output and error files located at* `WHERE?`
- QC topup quality 
    - Check alignment `fslview_deprecated EPI_scout_to_T2.nii.gz T2_masked.nii.gz`
    - Atlas registration `fslview_deprecated T2_to_ATLAS.nii.gz /home/exacloud/tempwork/fnl_lab/atlases/rat/EVA_TRANSFORM/ATLAS/EVA_TRANSFORM.nii.gz`
    - Within run registration `fslview_deprecated FWD_EPI#_final.nii.gz`
    - Between run registration `fslview_deprecated FWD_EPI#_final.nii.gz FWD_EPI#_final.nii.gz FWD_EPI#_final.nii.gz FWD_EPI#_final.nii.gz`


### Run through pipeline

- Edit command for `opto-fMRI_pipeline.sh` with option `-p` to run subject through pipeline

Example command:
```
/home/exacloud/tempwork/fnl_lab/code/internal/pipelines/opto-fMRI/opto-fMRI_pipeline.sh -o /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/output/PIKE02112019_RAT4303_ELEC_STIM/20190211 -s PIKE02112019_RAT4303_ELEC_STIM -p
```

- - *Output*: ``
- *Once job is complete check the output and error files located at* `WHERE?`
- *Check quality* `WHICHimages?`
---

