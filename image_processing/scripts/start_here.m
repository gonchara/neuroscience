clear all
clc
close all

%% add paths to where the scripts live
addpath('/mnt/max/shared/code/external/utilities/Matlab_nifti_and_ANALYZE'); %Matlab_nifti_and_ANALYZE
addpath('/mnt/max/shared/code/external/utilities/spm12/spm12'); %spm12
addpath('/mnt/max/shared/projects/OPTO_FMRI/alex_poplawsky_analysis/opto-fmri-spm/02_load_raw_Bruker-files_and_process'); %02_load_raw_Bruker-files_and_process

%% Set Directories
%mdir='/mnt/max/shared/projects/OPTO_FMRI/alex_poplawsky_analysis/'; % main directory
mdir='/mnt/max/shared/data/rodent/opto-fmri/input/';
time_dir='/mnt/max/shared/data/rodent/timing_files/'; % timing directory

%%
recov_ratio=0.5;
lin='no'; % 'yes' or 'no' for linear detrending of fMRI series after motion correction


%% s_2016100601
% sdir='s_2016100601'; % sub-directory where Bruker files are located%/mnt/max/shared/projects/OPTO_FMRI/alex_poplawsky_analysis/OptoRat_M212_20180626.O41 %OptoRat_M209_20180613.NR1
%mrid=[23:24];%[15:22];[15:23,25:33]; % Epi sequences
%mrid_stim={[23:24]};%{[19:20],[21:22],[19:22]};,[35:37] % Epi sequences w/ light stim % mrid_stim={[9:10],[11:12]};
%NOTES: EPIs need to have same dimensions 
sdir='PIKE07172019_MOUSE_OPTO_ELECTSTIM_7RT.Uj1'; 
mrid=[19:31]; 
mrid_stim={[19:26],[19:23],[24:26],[27:31]};

%%
auto_spm12_pre(mrid,strcat(mdir,sdir),recov_ratio,'epi','BOLD');  %comment out for pitt stats AG
%auto_spm12_pre(mrid,strcat(mdir,sdir),recov_ratio,'2DG','BOLD');  %AG Testing for T2


% Lite Motion Correction - Fast Process (3 Hours/sequence)
auto_spm12_SPMmotion(mrid,strcat(mdir,sdir),0.2, 24, 10, 4, lin); %MA determined this to work for OHSU data AG

% Strict Motion Correction - Very Slow process (3 days)
%auto_spm12_SPMmotion(mrid,strcat(mdir,sdir),0.8, 8, 10, 4, lin); %use this for pitt data AG

%%
% if timing file is to be used, enter 'yes' in the 4th argument

% This is for block of stimulus glm 
 %auto_spm12_glm_run(mrid_stim, mdir, sdir, 'yes', 'yes', time_dir); %uncomment 1/23/19 AG
% STATS
for i = 1:length(mrid_stim)
    % auto-naming the concatenated stimulus file outputs from the following function
    stim_filename = strrep(num2str(mrid_stim{i}),' ', '_') ; % Epi sequences w/ light stim
    % This concatonates all blocks from all sequences defined in mrid_stim for glm.
    auto_spm12_glm_runcat(mrid_stim{i}, mdir, sdir, 'yes', 'y',[],stim_filename, time_dir); %uncommented 9/13/18 AG
    %auto_spm12_glm_runcat(mrid_stim{i}, mdir, sdir, 'no', 'y',[],stim_filename, time_dir);
end

clear sdir mrid mrid_stim

