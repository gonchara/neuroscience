#!/bin/bash -e
#Update History
# 4/11/2016 BDM
# 12/12/17 AAG (Added comments)
# 4/10/18 AAD (adding T2_new)


#Script: Read the 1 argument, if exist, continue script. If not, display exit code 1
if [ $# -ne 2 ]; then

	echo -e "\nUsage:	`basename $0` <subject_directory> <group>"
	exit 1

fi

dir=$1 #1st argument is the subject directory
group=$2 #2nd argument is now the group to switch atlases
#sub=$2 #2nd argument is the subject ID
#type=$3 #3rd argument is the scan type
#atl=`readlink -f $3`
#atl_brain=`readlink -f $4`
if [ $group == mouse ]; then T2_REF=c57_11_M_Normal_age12_200200200.nii.gz; T2_REFMASK=c57_11_M_Normal_age12_mask_200200200.nii.gz; fi
if [ $group == rat ]; then T2_REF=T2_late2epi_ref.nii.gz; T2_REFMASK=T2_mask.nii.gz; fi

pushd $dir

if [ -f FLIRT.nii.gz ]; then
	echo "File FLIRT exists."
else
	echo "File FLIRT does not exist."
	#SCRIPT: Applies linear registration using FSL flirt. Places image into the same space as your atlas/reference image.
	#SCRIPT: command -input <path to image youre masking>  -reference  <path to reference image or atals image> -omat
	flirt -in ../T2_late2epi_d_bc.nii.gz -ref `dirname ${0}`/ANTS_FILES/${T2_REF} -omat FLIRT.mat -out FLIRT
fi

cp FLIRT.nii.gz rot2atl.nii.gz

#SCRIPT: Determines warp parameters for non linear transformation using ANTS with cross correlation method to place atlas/reference image mask into your image space.
#SCRIPT: ANTS  CC Cross corelation method;  -i downsamplings of image to register between. 60 permutations at top level 20 at lower --MI mutual info
ANTS 3 -m  CC[rot2atl.nii.gz,`dirname ${0}`/ANTS_FILES/${T2_REF},1,5] -t SyN[0.25] -r Gauss[.5,0] -o atl2T1rot -i 60x50x20 --use-Histogram-Matching  --number-of-affine-iterations 10000x10000x10000x10000x10000 --MI-option 32x16000
#Gauss was 3- changed to .5

#SCRIPT: Applies warp parameters to T2
antsApplyTransforms -d 3 -i `dirname ${0}`/ANTS_FILES/${T2_REFMASK} -t atl2T1rotWarp.nii.gz atl2T1rotAffine.txt -r rot2atl.nii.gz -o rot2atl_mask.nii.gz
#SCRIPT: Inverse transform atlas mask to T2
convert_xfm -omat orig_mat.mat -inverse FLIRT.mat
flirt -in rot2atl_mask -ref ../T2_late2epi_d_bc.nii.gz -o T2_mask_RBET_mask_unthr -applyxfm -init orig_mat.mat #-interp nearestneighbour

fslmaths T2_mask_RBET_mask_unthr -thr .5 -bin ../T2_mask_RBET_mask

rm -f rot*
rm -f atl2*
rm -f FLIRT*
rm -f orig_mat*

popd



#fslmaths ${sub} -mas ${sub}_mask ${sub}_brain

#ANTS 3 -m CC['/group_shares/PSYCH/atlases/Rat/EVA_TRANSFORM/ATLAS/EVA_TRANSFORM.nii.gz',FLIRT,1,5] -t SyN[0.25] -r Gauss[.5,0] -o 'ANTS' -i 50x90x30 --use-Histogram-Matching  --number-of-affine-iterations 10000x10000x10000x10000x10000 --MI-option 32x16000

#ANTS 3 -m  CC[FLIRT.nii.gz,/group_shares/FAIR_RODENT/Projects/Rat_misc/RAT_MASKING/ANTS_FILES/T2_late2epi_ref.nii.gz,1,5] -t SyN[0.25] -r Gauss[.5,0] -o atl2T1rot -i 60x50x20 --use-Histogram-Matching  --number-of-affine-iterations 10000x10000x10000x10000x10000 --MI-option 32x16000



## NOTE: antsApplyTransforms is deprecated in ANTS 2.0 and might not be available; if so, use WarpImageMultiTransform instead
#back it into native space; threshold it; apply it
#convert_xfm -omat rot2native.mat -inverse rot2atl.mat; flirt -in rot2atl_mask -ref ${sub} -o ${sub}_mask -applyxfm -init ${sub}_rot2native.mat
#fslmaths ${sub}_mask -thr .5 -bin ${sub}_mask
#fslmaths ${sub} -mas ${sub}_mask ${sub}_brain

