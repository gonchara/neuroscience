#!/bin/bash

# hardcoded scaling factor
SCALE=10

if [ $# -ne 2 ]; then
	echo -e "\nUsage:	`basename $0` <func_4Dimage> <brainmask>\n"
	echo -e "	Example: `basename $0` topup_applied_chpixdim.nii.gz topup_applied_swapYZ_brain_mask_edited.hdr\n"
	echo -e "	...Outputs topup_applied_chpixdim_mcf.nii.gz and topup_applied_chpixdim_scaleX<SCALE>_mcf.par file \n"
	exit 1
fi

func=`echo $1 | sed 's/\.nii\.gz$//' | sed 's/\.hdr$//'`
mask=`echo $2 | sed 's/\.nii\.gz$//' | sed 's/\.hdr$//'`

orientation=`fslorient -getorient $func`

if [ "$orientation" != NEUROLOGICAL ]; then
	echo "FATAL ERROR: Func 4D image $func is not in NEUROLOGICAL orientation. Good luck with that. GOODBYE."
	exit 1
fi

echo "mask dilation"
fslmaths $mask -dilF ${mask}dil

echo "applying dilated mask"
fslmaths $func -mas $mask ${func}_masked

echo "making rescaled image (scaled up by a factor of ${SCALE})"
fslmaths ${func}_masked ${func}_masked_scaleX${SCALE}
PIXDIM1_ORIG=`fslinfo ${func} | grep pixdim1 | awk '{print $2}'`
PIXDIM2_ORIG=`fslinfo ${func} | grep pixdim2 | awk '{print $2}'`
PIXDIM3_ORIG=`fslinfo ${func} | grep pixdim3 | awk '{print $2}'`
PIXDIM4_ORIG=`fslinfo ${func} | grep pixdim4 | awk '{print $2}'`
PIXDIM1_SCALED=`echo "${PIXDIM1_ORIG} * ${SCALE}" | bc`
PIXDIM2_SCALED=`echo "${PIXDIM2_ORIG} * ${SCALE}" | bc`
PIXDIM3_SCALED=`echo "${PIXDIM3_ORIG} * ${SCALE}" | bc`
fslchpixdim ${func}_masked_scaleX${SCALE} ${PIXDIM1_SCALED} ${PIXDIM2_SCALED} ${PIXDIM3_SCALED} ${PIXDIM4_ORIG}

echo "forcing back into neurological orientation (fslchpixdim resets to radiological)"
fslorient -setsformcode 1 ${func}_masked_scaleX${SCALE}
fslorient -forceneurological ${func}_masked_scaleX${SCALE}

echo "running mcflirt with final spline interpolation - I think requires fsl >=5.0"
mcflirt -in ${func}_masked_scaleX${SCALE} -meanvol -mats -plots -report -verbose 2 -spline_final -rmsrel -rmsabs # uses version /home/exacloud/tempwork/fnl_lab/code/external/utilities/fsl-5.0.10/bin/mcflirt
#fsl5.0-mcflirt -in ${func}_masked_scaleX${SCALE} -meanvol -mats -plots -report -verbose 2 -spline_final -rmsrel -rmsabs #named differently for airclogin-a1
fslmaths ${func}_masked_scaleX${SCALE}_mcf ${func}_masked_mcf
mv -f ${func}_masked_scaleX${SCALE}_mcf.par ${func}_scaleX${SCALE}_mcf.par
mv -f ${func}_masked_scaleX${SCALE}_mcf_rel.rms ${func}_scaleX${SCALE}_mcf_rel.rms
mv -f ${func}_masked_scaleX${SCALE}_mcf_abs.rms ${func}_scaleX${SCALE}_mcf_abs.rms

echo "rescaling MAT files back"
mkdir -p ${func}_masked_mcf.mat
fact=0.1
pushd ${func}_masked_scaleX${SCALE}_mcf.mat
for file in MAT_*; do
	dx=`awk 'NR==1 {print $4}' ${file}`
	dy=`awk 'NR==2 {print $4}' ${file}`
	dz=`awk 'NR==3 {print $4}' ${file}`
	chpix_dx=$(echo "scale=7; $dx * $fact" | bc)
	chpix_dy=$(echo "scale=7; $dy * $fact" | bc)
	chpix_dz=$(echo "scale=7; $dz * $fact" | bc)
	echo `awk 'NR==1 {print $1,$2,$3}' ${file}` ${chpix_dx} > ../${func}_masked_mcf.mat/${file}
	echo `awk 'NR==2 {print $1,$2,$3}' ${file}` ${chpix_dy} >> ../${func}_masked_mcf.mat/${file}
	echo `awk 'NR==3 {print $1,$2,$3}' ${file}` ${chpix_dz} >> ../${func}_masked_mcf.mat/${file}
	echo `awk 'NR==4' ${file}` >> ../${func}_masked_mcf.mat/${file}
done
popd

echo "rescaling 4D image back to original pixel dimensions"
fslchpixdim ${func}_masked_mcf ${PIXDIM1_ORIG} ${PIXDIM2_ORIG} ${PIXDIM3_ORIG} ${PIXDIM4_ORIG}

echo "forcing back into neurological orientation (fslchpixdim resets to radiological)"
fslorient -setsformcode 1 ${func}_masked_mcf
fslorient -forceneurological ${func}_masked_mcf

echo "re-masking with undilated mask"
fslmaths ${func}_masked_mcf -mas $mask ${func}_masked_mcf

echo "making timeseries plots of motion parameters"
fsl_tsplot -i ${func}_scaleX${SCALE}_mcf.par -t 'MCFLIRT estimated rotations (radians)' -u 1 --start=1 --finish=3 -a x,y,z -w 640 -h 144 -o rot.png
fsl_tsplot -i ${func}_scaleX${SCALE}_mcf.par -t 'MCFLIRT estimated translations (x${SCALE}^-1 mm)' -u 1 --start=4 --finish=6 -a x,y,z -w 640 -h 144 -o trans.png
fsl_tsplot -i ${func}_scaleX${SCALE}_mcf_abs.rms,${func}_scaleX${SCALE}_mcf_rel.rms -t 'MCFLIRT estimated mean displacement (x${SCALE}^-1 mm)' -u 1 -w 640 -h 144 -a absolute,relative -o disp.png

echo "running dvars calculator and plot maker"
fsl_motion_outliers -i ${func}_masked_mcf -o spike_outlier_timepoints.txt --dvars --nomoco -m $mask -s DVARS.txt -p DVARS_plot

rm -rf ${func}_masked_scaleX${SCALE}_mcf.nii.gz
exit
