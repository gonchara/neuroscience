function baphy_make_timing_file(baphy_file) %add to path /home/exacloud/tempwork/

run(baphy_file)
day2sec_factor = 1 * 24 * 60 * 60;
baphytable = struct2table(exptevents);
beginning = day2sec_factor * datenum( baphytable.Note{1}(14:end) ) ;

output_text = [baphy_file '_timing_file.txt'];
fid = fopen(output_text,'w');

for i = 1:height(baphytable) 
    if strcmp(baphytable.Note{i},'PreStimSilence , Silence , Reference+Light') 
        starttime = day2sec_factor * datenum( baphytable.Note{i-1}(14:end) ) - beginning ;
        prestimduration = baphytable.StopTime(i) - baphytable.StartTime(i) ;
        stimduration = baphytable.StopTime(i+1) - baphytable.StartTime(i+1) ;
        stimstart = starttime + prestimduration ;
%         %%% UNNEEDED %%%
%         poststimduration = baphytable.StopTime(i+2) - baphytable.StartTime(i+2) ;
%         stimstop = stimstart + stimduration ;
        fprintf(fid,'%0.3f\t%0.3f\t1\n',stimstart,stimduration);
    end
end

fclose(fid);