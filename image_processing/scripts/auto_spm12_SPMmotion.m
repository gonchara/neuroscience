function auto_spm12_SPMmotion(mrid,direct1,mc_quality, mc_sep, mc_fwhm, mc_interp, lin_detrend)

ff = filesep;

if ~exist('lin_detrend','var')
    lin_detrend='yes';
end

%% Define Folders

direct2=strcat(direct1,ff,'PreProcess');
tdirect=strcat(direct2,ff,'temp');

%% Motion Correct

mc.p.quality = mc_quality;
mc.p.sep = mc_sep;
mc.p.fwhm = mc_fwhm;
mc.p.interp = mc_interp;

if ~exist(tdirect,'dir')
    mkdir(tdirect);
end
cd(tdirect)

for mm=1:length(mrid)
    
    fname=strcat(direct2,ff,num2str(mrid(mm),'%02d'));
    load(fname,'raw','raw_fmri','f');   
    
    % comment out for OHSU data. MA
    % raw_fmri=reshape(raw_fmri,[f.np f.nv f.ns f.nt]);
    
    %%% Make NifTI files
    sdirect=strcat(tdirect,ff,num2str(mrid(mm),'%02d'));
    if ~exist(sdirect,'dir')
        mkdir(sdirect);
    end
    cd(sdirect)
    
    fname2=cell(f.nt,1);
    for nn=1:f.nt
        fname1=strcat('t',sprintf('%04d',nn),'.nii');
        fname2(nn,:)=strcat({sdirect},ff,fname1);
        
        if ~exist(fname1,'file')
            nii=make_nii(raw_fmri(:,:,:,nn),f.sz_mult*[f.fov(1)/f.np f.fov(2)/f.nv f.thk]);
            save_nii(nii,fname1);
        end

        suff=',1';
        fname2(nn,:)=strcat(fname2(nn,:),{suff});  
    end    
    
    matlabbatch{1}.spm.spatial.realign.estwrite.data{1,mm}=fname2;
    clear fname2
end

if strcmp(f.pos,'Head_Supine') && strcmp(f.read_dir,'L_R') && strcmp(f.ns_orient,'axial')
    wrap=[1,0,0];
elseif strcmp(f.pos,'Head_Supine') && strcmp(f.read_dir,'H_F') && strcmp(f.ns_orient,'coronal')
    wrap=[1,0,0];
else
    wrap=[1,0,0];
%     error('Update wrapping direction here')
end

%%% SPM realign
spm('defaults','fmri');
spm_jobman('initcfg');

matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.quality=mc.p.quality;
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.sep=mc.p.sep;
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.fwhm=mc.p.fwhm;
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.rtm=1;
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.interp=mc.p.interp;
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.wrap=wrap;
matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.weight='';

matlabbatch{1}.spm.spatial.realign.estwrite.roptions.which=[2,0];
matlabbatch{1}.spm.spatial.realign.estwrite.roptions.interp=7;
matlabbatch{1}.spm.spatial.realign.estwrite.roptions.wrap=wrap;
matlabbatch{1}.spm.spatial.realign.estwrite.roptions.mask=0;
matlabbatch{1}.spm.spatial.realign.estwrite.roptions.prefix='mc';

spm_jobman('run',matlabbatch)
clear matlabbatch

%% Save realigned files to matlab
for mm=1:length(mrid)   
    fname=strcat(direct2,ff,num2str(mrid(mm),'%02d'));
    load(fname,'f');
    
    sdirect=strcat(tdirect,ff,num2str(mrid(mm),'%02d'));
    cd(sdirect)
    
    % mc_fmri=zeros(f.np,f.nv,f.ns,f.nt);
    % changing this due to commenting out the reshpae above. this could be automated. MA  
    mc_fmri=zeros(f.nv,f.np,f.ns,f.nt); %ns # of slices. nt # of timepoints
    
    for nn=1:f.nt
        fname1=strcat(sdirect,ff,'mct',sprintf('%04d',nn),'.nii');%Rushmore %fname1=strcat(sdirect,'\mct',sprintf('%04d',nn),'.nii'); %Orig
        t=load_nii(fname1);
        mc_fmri(:,:,:,nn)=t.img; clear t
    end
    
    
    if strcmp(lin_detrend,'yes')
        %%% Remove linear drift
        f.lin_detrend='yes';
        mc_fmri=reshape(permute(mc_fmri,[4 1 2 3]),f.nt,f.np*f.nv*f.ns);
        mc_fmri=mydetrend(mc_fmri,f.Sbaseline);
        mc_fmri=reshape(permute(mc_fmri,[2 3 4 1]),f.np,f.nv,f.ns,f.nt);
%         figure; imagesc(reshape(raw_fmri(:,:,:,50),f.np,f.nv*f.ns)); axis image; colormap gray
    else
        f.lin_detrend='no';
    end
        
    mc.mp=load(strcat(sdirect,ff,'rp_t0001.txt'));    
    mc.mean=mean(mc_fmri(:,:,:,f.Sbaseline),4);
    
    save(fname,'f','mc','mc_fmri','-append');
end
cd(direct1)

