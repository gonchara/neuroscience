function auto_spm12_glm_run(mrid, mdir, sdir, use_timing_file, mp_regress, gamma_cbv_choice, time_dir)

ff = filesep;

if ~exist('gamma_cbv_choice','var')
    gamma_cbv_choice=1;
end

if ~exist('time_dir','var')
    time_dir='n';
end
%% Define Folders
direct1 = strcat(mdir,sdir);

direct2=strcat(direct1,ff,'PreProcess');

tdirect=strcat(direct2,ff,'temp');

%%
for mm=1:length(mrid)
    
    %%% Make NifTI files
    sdirect=strcat(tdirect,ff,num2str(mrid(mm),'%02d'),ff)%;
    if ~exist(sdirect,'dir')
        mkdir(sdirect);
    end
    cd(sdirect)
    
    fname=strcat(direct2,ff,num2str(mrid(mm),'%02d'));
    load(fname,'grp','mc','mc_fmri','f');
    
    % This was added to implement OHSU timing files. MA. You dont need to
    % comment this out
    if strcmp(use_timing_file,'yes')
        seq = num2str(mrid(mm))
        file_timing = [time_dir, sdir, ff, seq, '.txt'];
        if exist(file_timing, 'file') == 2
            timing = importdata(file_timing);
            nt_stim_on = round(timing(1,2)/f.tr);
            f.sli = zeros(1,f.nt);
            for t=1:length(timing)
                s = round(timing(t,1)/f.tr);
                f.sli(s+1 : s+nt_stim_on) = 1;
            end
            save(fname,'grp','mc','mc_fmri','f');
        else
            error('use_timing_file is "yes", if you want to use a timing file, make sure it is in the timing directory named "{sequence_num}_{subject_ID}.txt" e.g. 15_OptoRat_M212_20180606.NK1.txt')
        end
    end

    d_sli1=diff(f.sli);  % derivative to find sli1 transition points
    on=find(d_sli1==1)+1;   % 1st TR that is ON, assumes onset occurs when sli1 goes from 0 --> 1
    off=find(d_sli1==-1)+1; % 1st TR that is OFF, assumes offset occurs when sli1 goes from 1 --> 0
    dur=off-on-1;           % Duration of sli1 (-1 to remove spm12 automatically adds another TR at end of stimulus before convolusion with HRF to account for sustained activity (See "spm_get_ons.m", line 128, variable "tof"). 
     
    fname2=cell(f.nt,1);
    for nn=1:f.nt
        fname1=strcat('mct',sprintf('%04d',nn),'.nii'); 
        if ~exist(fname1,'file')
            nii=make_nii(mc_fmri(:,:,:,nn),f.sz_mult*[f.fov(1)/f.np f.fov(2)/f.nv f.thk]);
            save_nii(nii,fname1);
        end
        fname2(nn,:)={strcat(sdirect,fname1,',1')};  
    end    
    
    clear mc mc_fmri
    fname3=cellstr(fname2);
    
    %%%% Collapse fMRI data into single stimulations
    nblock=length(on);
    nt_block=f.nt/nblock;
    if nblock>1
        on=on(1); dur=dur(1);
    end
        
    
    %% fMRI Stats
    spm('defaults','fmri');
    spm_jobman('initcfg');
    
    oo=1;
    for nn=1:nblock
        fname2=fname3(oo:oo+nt_block-1,:);
        
        if exist('SPM.mat','file')==2
            delete('SPM.mat')
        end
        
        matlabbatch{1}.spm.stats.fmri_spec.dir={sdirect};
        
        matlabbatch{1}.spm.stats.fmri_spec.timing.units='scans';
        matlabbatch{1}.spm.stats.fmri_spec.timing.RT=f.tr;
        matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t=1;  % 1 = no microtime (only used with slice timing correction)
        matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0=1; % 1 = no microtime (only used with slice timing correction)
        
        matlabbatch{1}.spm.stats.fmri_spec.sess.scans=fname2;
        matlabbatch{1}.spm.stats.fmri_spec.sess.cond.name='STIM';
        matlabbatch{1}.spm.stats.fmri_spec.sess.cond.onset=on';
        matlabbatch{1}.spm.stats.fmri_spec.sess.cond.duration=dur';
        matlabbatch{1}.spm.stats.fmri_spec.sess.cond.tmod=0;
        matlabbatch{1}.spm.stats.fmri_spec.sess.cond.pmod=struct([]);
        matlabbatch{1}.spm.stats.fmri_spec.sess.cond.orth=1; %%%
        matlabbatch{1}.spm.stats.fmri_spec.sess.multi={''};
        
%         matlabbatch{1}.spm.stats.fmri_spec.sess.regress=struct([]);
        %%% Turn on below if want to include linear drift as a regressor
            matlabbatch{1}.spm.stats.fmri_spec.sess.regress.name='LinDrift';
            matlabbatch{1}.spm.stats.fmri_spec.sess.regress.val=0:1/(f.nt/nblock-1):1;
        
        if strcmp(mp_regress,'y')
            matlabbatch{1}.spm.stats.fmri_spec.sess.multi_reg={strcat(sdirect,'rp_t0001.txt')};
            if nblock>1
                mp_temp=load(strcat(sdirect,'rp_t0001.txt'));
                mp_temp=mp_temp(oo:oo+nt_block-1,:)';
                fileID = fopen('mp_temp.txt','w');
                fprintf(fileID,'%16.7e %16.7e %16.7e %16.7e %16.7e %16.7e\r\n',mp_temp);
                fclose(fileID);
                matlabbatch{1}.spm.stats.fmri_spec.sess.multi_reg={strcat(sdirect,'mp_temp.txt')};
            
            end
        else
            matlabbatch{1}.spm.stats.fmri_spec.sess.multi_reg={''};
        end
        
        matlabbatch{1}.spm.stats.fmri_spec.sess.hpf=10000;
        matlabbatch{1}.spm.stats.fmri_spec.fact=struct([]);
        
        if strcmp(grp,'CBV')==1
            matlabbatch{1}.spm.stats.fmri_spec.bases.gamma.length=50; % (s), length of impulse response curve
            matlabbatch{1}.spm.stats.fmri_spec.bases.gamma.order=gamma_cbv_choice;   % chooses gamma custom gamma functions in gamma_cbv.m 
            matlabbatch{1}.spm.stats.fmri_spec.sess.cond.onset=on'-1; % 2/15/2018, spm12 automatically delays onset by 1 s. Subtracting 1 removes this delayed onset by spm
            glm.p.gamma_cbv_choice=gamma_cbv_choice;
        elseif strcmp(grp,'FIR')
            matlabbatch{1}.spm.stats.fmri_spec.bases.fir.length=1; % (s), length of impulse response curve
            matlabbatch{1}.spm.stats.fmri_spec.bases.fir.order=1;   
        elseif strcmp(grp,'BOLD')==1
            matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs=[0,0]; % Canonical HRF
        end
        
        matlabbatch{1}.spm.stats.fmri_spec.volt=1;
        matlabbatch{1}.spm.stats.fmri_spec.global='None';
        matlabbatch{1}.spm.stats.fmri_spec.mthresh=0; %%%
        matlabbatch{1}.spm.stats.fmri_spec.mask={''};
        matlabbatch{1}.spm.stats.fmri_spec.cvi='AR(1)';
        
        spm_jobman('run',matlabbatch)
        clear matlabbatch
        
        fname2='SPM.mat';
        matlabbatch{1}.spm.stats.fmri_est.spmmat=strcat(sdirect,{fname2});
        matlabbatch{1}.spm.stats.fmri_est.method.Classical=1;
        
        spm_jobman('run',matlabbatch)
        clear matlabbatch
        
        %% Generating Contrast (T) maps
        
        matlabbatch{1}.spm.stats.con.spmmat=strcat(sdirect,{fname2});
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.name='STIM';
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.convec=[1,0];
        matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep='none';
        matlabbatch{1}.spm.stats.con.delete=1;
        
        spm_jobman('run',matlabbatch)
        clear matlabbatch
        
        
        %% Read SPM Files and Save to Matlab
        
        load(strcat(sdirect,'SPM'));
        glm.p.design_mat(:,:,nn)=SPM.xX.X;
        glm.p.df(:,nn)=size(SPM.xX.X,1)-size(SPM.xX.X,2);        
        
        %%% T-map
        glm.tmap(:,:,:,nn)=spm_read_vols(spm_vol('spmT_0001.nii'));
        glm.tmap(isnan(glm.tmap))=0;
        
        %%% Beta Maps
        for pp=1:size(glm.p.design_mat,2)
            glm.beta(:,:,:,pp,nn)=spm_read_vols(spm_vol(strcat('beta_',sprintf('%04d',pp),'.nii')));            
        end
        glm.beta(isnan(glm.beta))=0;
        
        %%% Percent Change Map
        glm.percent(:,:,:,nn)=100*(glm.beta(:,:,:,1,nn)./glm.beta(:,:,:,end,nn));
        glm.percent(isnan(glm.percent))=0;
        
        clear fname2 mp_temp
        oo=oo+nt_block;
        
    end
    
    save(fname,'glm','-append');    
    clear fname3 sdirect tr1 modality glm
    
end
cd(direct1)