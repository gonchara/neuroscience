function auto_spm12_glm_runcat(mrid,mdir,sdir,use_timing_file,mp_regress,gamma_cbv_choice,suffix,time_dir)

ff = filesep;

if ~exist('gamma_cbv_choice','var')
    gamma_cbv_choice=1;
end

if ~exist('time_dir','var')
    time_dir='n';
end

%% Define Folders
direct1 = strcat(mdir,sdir);

direct2=strcat(direct1, ff, 'PreProcess');

tdirect=strcat(direct2, ff, 'temp');

tcat_direct=strcat(direct2, ff, 'temp', ff, suffix, ff);
if ~exist(tcat_direct,'dir')
        mkdir(tcat_direct);
end

%%
glm.p.sli=[];
glm.p.mp=[];
pp=0;
for mm=1:length(mrid)
    
    %%% Make NifTI files
    sdirect=strcat(tdirect,ff,num2str(mrid(mm),'%02d'),ff);
    if ~exist(sdirect,'dir')
        mkdir(sdirect);
    end
    cd(sdirect)
    
    fname=strcat(direct2,ff,num2str(mrid(mm),'%02d'));
    load(fname,'grp','mc','mc_fmri','f'); 
    
    % This was added to implement OHSU timing files. MA. You dont need to
    % comment this out
    if strcmp(use_timing_file,'yes')
        seq = num2str(mrid(mm))
        file_timing = [time_dir, sdir, ff, seq, '.txt'];
        if exist(file_timing, 'file') == 2
            timing = importdata(file_timing);
            nt_stim_on = round(timing(1,2)/f.tr);
            f.sli = zeros(1,f.nt);
            for t=1:length(timing)
                s = round(timing(t,1)/f.tr);
                f.sli(s+1 : s+nt_stim_on) = 1;
            end
            save(fname,'grp','mc','mc_fmri','f');
        else
            error('use_timing_file is "yes", if you want to use a timing file, make sure it is in the timing directory named "{sequence_num}_{subject_ID}.txt" e.g. 15_OptoRat_M212_20180606.NK1.txt')
        end
    end
    
    glm.p.sli=cat(2,glm.p.sli,f.sli);
    glm.p.mp=cat(1,glm.p.mp,mc.mp);
%     sli_check(mm)=f.sli_check;
    grp_check(mm)={grp};
    
    mean_all(:,:,:,mm)=mc.mean;
    
    for nn=1:f.nt
        fname1=strcat('mct',sprintf('%04d',nn),'.nii');
        if ~exist(fname1,'file')
            nii=make_nii(mc_fmri(:,:,:,nn),f.sz_mult*[f.fov(1)/f.np f.fov(2)/f.nv f.thk]);
            save_nii(nii,fname1);
        end        
        pp=pp+1;
        fname2(pp,:)={strcat(sdirect,fname1,',1')};        
    end  
    clear mc mc_fmri
end

% sli_check=unique(sli_check);
% if length(sli_check)>1
%     error('Sli_checks were not consistent');
% end

grp_check=unique(grp_check);
if length(grp_check)>1
    error('Groups were not consistent');
end
grp_check=grp_check{:};

dlmwrite(strcat(tcat_direct,'cat_mp.txt'),glm.p.mp);

fname2=cellstr(fname2);

d_sli1=diff(glm.p.sli);  % derivative to find sli1 transition points
on=find(d_sli1==1)+1;   % 1st TR that is ON, assumes onset occurs when sli1 goes from 0 --> 1
off=find(d_sli1==-1)+1; % 1st TR that is OFF, assumes offset occurs when sli1 goes from 1 --> 0
dur=off-on-1;           % Duration of sli1 (-1 to remove spm12 automatically adds another TR at end of stimulus before convolusion with HRF to account for sustained activity (See "spm_get_ons.m", line 128, variable "tof"). 

    
    %% fMRI Stats
    spm('defaults','fmri');
    spm_jobman('initcfg');
    
    if exist(strcat(tcat_direct, ff, 'SPM.mat'),'file')==2
        delete(strcat(tcat_direct, ff, 'SPM.mat'))
    end
    
    matlabbatch{1}.spm.stats.fmri_spec.dir={tcat_direct};
    
    matlabbatch{1}.spm.stats.fmri_spec.timing.units='scans';
    matlabbatch{1}.spm.stats.fmri_spec.timing.RT=f.tr;
    matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t=1;  % 1 = no microtime (only used with slice timing correction)
    matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0=1; % 1 = no microtime (only used with slice timing correction)
   
    matlabbatch{1}.spm.stats.fmri_spec.sess.scans=fname2;
    matlabbatch{1}.spm.stats.fmri_spec.sess.cond.name='STIM';
    matlabbatch{1}.spm.stats.fmri_spec.sess.cond.onset=on';
    matlabbatch{1}.spm.stats.fmri_spec.sess.cond.duration=dur';
    matlabbatch{1}.spm.stats.fmri_spec.sess.cond.tmod=0; 
    matlabbatch{1}.spm.stats.fmri_spec.sess.cond.pmod=struct([]);   
    matlabbatch{1}.spm.stats.fmri_spec.sess.cond.orth=1; %%%    
    matlabbatch{1}.spm.stats.fmri_spec.sess.multi={''};
    
%     matlabbatch{1}.spm.stats.fmri_spec.sess.regress=struct([]);
    %%% Turn on below if want to include linear drift as a regressor
    matlabbatch{1}.spm.stats.fmri_spec.sess.regress.name='LinDrift';
    matlabbatch{1}.spm.stats.fmri_spec.sess.regress.val=0:1/(length(glm.p.sli)-1):1;
    
    if strcmp(mp_regress,'y')
        matlabbatch{1}.spm.stats.fmri_spec.sess.multi_reg={strcat(tcat_direct,'cat_mp.txt')};
    else        
        matlabbatch{1}.spm.stats.fmri_spec.sess.multi_reg={''};
    end
    matlabbatch{1}.spm.stats.fmri_spec.sess.hpf=10000;
    
    matlabbatch{1}.spm.stats.fmri_spec.fact=struct([]);
    
    if strcmp(grp,'CBV')==1
        matlabbatch{1}.spm.stats.fmri_spec.bases.gamma.length=50; % (s), length of impulse response curve
        matlabbatch{1}.spm.stats.fmri_spec.bases.gamma.order=gamma_cbv_choice;   % chooses gamma custom gamma functions in gamma_cbv.m
        matlabbatch{1}.spm.stats.fmri_spec.sess.cond.onset=on'-1; % 2/15/2018, spm12 automatically delays onset by 1 s. Subtracting 1 removes this delayed onset by spm
        glm.p.gamma_cbv_choice=gamma_cbv_choice;
    elseif strcmp(grp,'FIR')
        matlabbatch{1}.spm.stats.fmri_spec.bases.fir.length=1; % (s), length of impulse response curve
        matlabbatch{1}.spm.stats.fmri_spec.bases.fir.order=1;
    elseif strcmp(grp,'BOLD')==1
        matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs=[0,0]; % Canonical HRF
    end
    
    matlabbatch{1}.spm.stats.fmri_spec.volt=1;
    matlabbatch{1}.spm.stats.fmri_spec.global='None';
    matlabbatch{1}.spm.stats.fmri_spec.mthresh=0;
    matlabbatch{1}.spm.stats.fmri_spec.mask={''};
    matlabbatch{1}.spm.stats.fmri_spec.cvi='AR(1)';
    
    spm_jobman('run',matlabbatch)
    clear matlabbatch   
    
    fname2='SPM.mat';
    matlabbatch{1}.spm.stats.fmri_est.spmmat=strcat(tcat_direct,{fname2});
    matlabbatch{1}.spm.stats.fmri_est.method.Classical=1;
    
    spm_jobman('run',matlabbatch)    
    clear matlabbatch
    
    %% Generating Contrast (T) maps
    
    matlabbatch{1}.spm.stats.con.spmmat=strcat(tcat_direct,{fname2});
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.name='STIM';
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.convec=[1,0];
    matlabbatch{1}.spm.stats.con.consess{1}.tcon.sessrep='none';
    matlabbatch{1}.spm.stats.con.delete=1;    
    
    spm_jobman('run',matlabbatch)
    clear matlabbatch
    
    
    %% Read SPM Files and Save to Matlab
    cd(tcat_direct)
    
    load(strcat(tcat_direct,'SPM')); 
    glm.p.design_mat=SPM.xX.X;
    glm.p.df=size(SPM.xX.X,1)-size(SPM.xX.X,2);
    
    
    %%% T-map
    glm.tmap=spm_read_vols(spm_vol('spmT_0001.nii'));
    glm.tmap(isnan(glm.tmap))=0;
    
    %%% Beta Maps    
    for nn=1:size(glm.p.design_mat,2)
        glm.beta(:,:,:,nn)=spm_read_vols(spm_vol(strcat('beta_',sprintf('%04d',nn),'.nii')));
        glm.beta(isnan(glm.beta))=0;        
    end
    
    %%% Percent Change Map
    glm.percent=100*(glm.beta(:,:,:,1)./glm.beta(:,:,:,end));
    glm.percent(isnan(glm.percent))=0;    
    
    %%% Mean functional underlay
    mc.mean=mean(mean_all,4);
    
%     save(strcat(direct1,ff,'cat_',grp,'_',suffix),'mc','glm','mrid','sli_check','grp_check');
    save(strcat(direct1,ff,'cat_',grp,'_',suffix),'mc','glm','mrid','grp_check');
    
    clear fname2 sdirect tr1 modality glm
    
cd(direct1)