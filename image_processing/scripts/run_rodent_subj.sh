#!/bin/bash

#Written By: Dakota Ortega
#Date Created; 5/21/2018
#Description: Convert bruker data to nifti format. Run subject through pipeline.

TEMP=`getopt -o ho:s:b:t:e:r:n:d:fuwcj:g:z:x:mp --long help,output:,subject:,brukerdata:,t2:,epi:,revepi:,revnum:,dfm:,rundfm,topup,pseudo,correl,roijson:,group:,dwell:,tr:,masktopup,pipeline \
-n 'test_bash.sh' -- "$@"`
eval set -- "$TEMP"

while true ; do
  case ${1} in
    -h|--help) echo -e "\nUsage:\n\n Required Args:\n"
        echo -e "\t-o,--output [<output directory> Path to subject directory. (Ex. .../processed/scan_type/subject/date)]"
        echo -e "\t-s,--subject [<subject id> Subject ID (Ex. OptoRat_M314_20180420).]\n\n Options:\n"
        echo -e "\t-h,--help [usage help]"
        echo -e "\t-b,--brukerdata [<path to bruker data directory> Pulls and converts bruker data to nifti format.]"
        echo -e "\t-t,--t2 [<T2w folder number> Pull T2w to output directory then Denoise and BiasField Correct.]"
        echo -e "\t-e,--epi [<List of EPI folder numbers as a string (Ex. \"10 11 12\")> Pull EPIs to output directory.]"
        echo -e "\t-r,--revepi [<Reverse EPI bruker path> Convert REV_EPI bruker data to niftis.]"
        echo -e "\t-n,--revnum [<Reverse EPI folder number> Pull REV_EPI to output directory.]"
        echo -e "\t-d,--dfm [<DFM folder number> Pull DFM to output directory. Creates MAG and PHASE niftis then masks MAG. **Once the edited MAG mask is created, rerun this step to run correction script.**]"
        echo -e "\t-f,--rundfm [Run DFM Pipeline]"
        echo -e "\t-u,--topup [Run Topup]" 
        echo -e "\t-w,--pseudo [Run Pseudo Topup. Replaces topup_applied_chpixdim with FWD_EPIs.]"
        echo -e "\t-c,--correl [Run Seed Correlations for functional connectivity analyses. If DFM was ran, add the -f option]" 
        echo -e "\t-j,--roijson [When you're ready to run Seed Correlations, only add this option with your new ROI_Set json path to change the default (.../ROI_sets/Atlas_based/Rat/SHWARZ_FUNC.json).]" 
        echo -e "\t-g,--group [The default group is rat (rt). Add this option and type ms to change the default to mouse.]"
        echo -e "\t-z,--dwell [The default dwell time is 0.256. Add this option with your new value to change the default.]"
        echo -e "\t-x,--tr [If you would like to change the default TR of 2.5, add this option with your new tr value.]"
        echo -e "\t-m,--masktopup [Run Topup Mask]"
        echo -e "\t-p,--pipeline [Run Pipeline]\n" ;
        exit 1 ;;
    -o|--output) output_dir=$2 ; shift 2 ;;
    -s|--subject) subject_id=$2 ; shift 2 ;;
    -b|--brukerdata) bruker_data_dir=$2 ; shift 2 ;;
    -t|--t2) T2w_number=$2 ; shift 2 ;;
    -e|--epi) EPI_num_array=($2) ; shift 2 ;;
    -r|--revepi) REV_EPI_Path=$2; shift 2 ;;
    -n|--revnum) REV_EPI_number=$2; shift 2 ;;
    -d|--dfm) dfm_number=$2; shift 2 ;;
    -f|--rundfm) run_dfm=true; shift ;;
    -u|--topup) run_topup=true; shift ;;
    -w|--pseudo) pseudo_topup=true; shift ;;
    -c|--correl) run_correl=true; shift ;;
    -j|--roijson) roi_set_json=$2; shift 2 ;;
    -g|--group) group=$2; shift 2 ;;
    -z|--dwell) dwell=$2; shift 2 ;;
    -x|--tr) tr=$2; shift 2 ;;
    -m|--masktopup) mask_topup=true; shift ;;
    -p|--pipeline) run_pipeline=true; shift ;;
    --) shift ; break ;;
    *) echo "ERROR: Unrecognized Option" ; exit 1 ;;
  esac
done

#Required Args Check
if [ -z "${output_dir}" ] || [ -z "${subject_id}" ]; then
  echo -e "\n ERROR: One or more of these arguments not defined.\n Required Args:\n\t-o [output directory]\n\t-s [subject id]\n"
  exit 1
fi

#If directory doesn't exist, create directory.
if [ ! -d "${output_dir}" ]; then
  mkdir -p ${output_dir}
fi

#Section commented out. Dont need sorted anymore. Not using CYA
#sorted_dir=$(echo $output_dir | sed 's/processed/sorted/g')
#if [ ! -d "${sorted_dir}" ]; then
#  mkdir -p ${sorted_dir}
#fi

#Path to bruker2nifti.
b2n="/home/exacloud/tempwork/fnl_lab/code/external/utilities/anaconda2/bin"
#Path to ANTs masking
mask_path="/home/exacloud/tempwork/fnl_lab/code/internal/pipelines/opto-fMRI"
#Path to pipeline wrappers
wrapper_path="/home/exacloud/tempwork/fnl_lab/code/internal/pipelines/nipype/wrappers"
#Path to rodent scripts
dfm_split="/home/exacloud/tempwork/fnl_lab/code/internal/utilities/rodent/rat_pull_data"

#Run bruker2nifiti
if [ -n "${bruker_data_dir}" ]; then
  ${b2n}/bruker2nifti -i ${bruker_data_dir} -o ${output_dir} -study_name "b2n_data" -nifti_version 1 -qform_code 1 -sform_code 1 -correct_slope
fi

#Copy and rename T2w from b2n to output directory then Denoise and Biasfield correct.
if [ -n "${T2w_number}" ]; then
  echo "Copying T2 from series: ${T2w_number}"
  cp ${output_dir}/b2n_data/b2n_data_${T2w_number}/b2n_data_${T2w_number}.nii.gz ${output_dir}/T2_late2epi.nii.gz
  srun --mem 5G -t 02:00:00 DenoiseImage -d 3 -i ${output_dir}/T2_late2epi.nii.gz -n Rician -o ${output_dir}/T2_late2epi_d.nii.gz
  srun --mem 5G -t 02:00:00 N4BiasFieldCorrection -d 3 -i ${output_dir}/T2_late2epi_d.nii.gz -o ${output_dir}/T2_late2epi_d_bc.nii.gz
  
  #Create T2w Mask
  if [ ! -f "${output_dir}/T2_mask_RBET_mask.nii.gz" ]; then
    srun --mem 8G -t 07:00:00 ${mask_path}/MASK_ANTS_T2_d_bc ${output_dir} ${subject_id} 'T2'
  fi
fi

#Copy and rename EPIs from b2n to output directory.
index=1
if [ -n "${EPI_num_array}" ]; then
  for i in "${EPI_num_array[@]}"; do
    echo "Copying EPI from series: ${i}"
    cp -f ${output_dir}/b2n_data/b2n_data_${i}/b2n_data_${i}.nii.gz ${output_dir}/FWD_EPI${index}.nii.gz
    index=$(($index+1))
  done
fi

#Run bruker2nifti on REV_EPI path
if [ -n "${REV_EPI_Path}" ]; then
  ${b2n}/bruker2nifti -i ${REV_EPI_Path} -o ${output_dir} -study_name "b2n_rev_data" -nifti_version 1 -qform_code 1 -sform_code 1 -correct_slope
fi

#Copy and rename REV_EPI from b2n to output directory.
if [ -n "${REV_EPI_number}" ]; then
  echo "Copying REV_EPI from series: ${REV_EPI_number}"
  cp -f ${output_dir}/b2n_rev_data/b2n_rev_data_${REV_EPI_number}/b2n_rev_data_${REV_EPI_number}.nii.gz ${output_dir}/REV_EPI.nii.gz
fi

#Copy and rename DFM from b2n to output directory. Run DFM_split.sh to create MAG and PHASE niftis. Mask MAG nifti.
#Once the edited MAG mask is created, rerun this step to run fezbrian_DFM_correction.sh on the edited MAG mask.
if [ -n "${dfm_number}" ]; then
  pushd ${output_dir} > /dev/null
  #If DFM doesn't exist, pull to output directory
  if [ ! -f "${output_dir}/DFM.nii.gz" ]; then
    cp ${output_dir}/b2n_data/b2n_data_${dfm_number}/b2n_data_${dfm_number}.nii.gz ${output_dir}/DFM.nii.gz
  fi
  
  #If the MAG mask doesn't exist, mask MAG.nii.gz
  if [ ! -f "${output_dir}/MAG_mask.nii.gz" ]; then
    ${dfm_split}/DFM_split_edited.sh DFM.nii.gz ${subject_id} ${output_dir}
  fi
  
  #If the edited MAG mask exists, run fezbrian_DFM_correction.sh
  if [ -f "${output_dir}/MAG_mask_EDIT.hdr" ] || [ -f "${output_dir}/MAG_mask_EDIT.nii.gz" ]; then
    #If edited mask is a nifti, convert to .hdr/.img
    if [ -f "${output_dir}/MAG_mask_EDIT.nii.gz" ];then
      fslchfiletype NIFTI_PAIR MAG_mask_EDIT.nii.gz
    fi
    
    #Run correction script
    echo "Running fezbrian_DFM_correction.sh script"
    RECO_map_slope=783.455467294033
    ${dfm_split}/fezbrian_DFM_correction.sh ${output_dir} MAG_mask_EDIT.hdr ${RECO_map_slope}
    echo "Correction step complete"
    
    #Convert edited mask to nifti
    fslchfiletype NIFTI_GZ MAG_mask_EDIT.img
    popd > /dev/null
  fi
fi

#Run Topup mask
if ${mask_topup:-false}; then
  #Create Topup mask
  srun --mem 8G -t 07:00:00 ${mask_path}/MASK_ANTS_TOP_UP ${output_dir}/FAIRPRE_FSL_13_RAT_TOPUP_LIN/TOPUP_2_FUNC1/8nFUNC_chpx
  
  #Copy Topup nifti to output directory
  cp ${output_dir}/FAIRPRE_FSL_13_RAT_TOPUP_LIN/TOPUP_2_FUNC1/8nFUNC_chpx/topup_applied_chpixdim.nii.gz ${output_dir}
fi

#Run Topup, Pseudo Topup, DFM Pipeline, or Topup Pipeline
if ${run_topup:-false} || ${pseudo_topup:-false} || ${run_dfm:-false} || ${run_pipeline:-false}; then
  pushd ${output_dir} > /dev/null
  
  #Convert T2 mask .hdr/.img to nifti
  if [ -f "${output_dir}/T2_mask_RBET_mask_EDIT.img" ]; then
    fslchfiletype NIFTI_PAIR T2_mask_RBET_mask_EDIT.img
  fi
  
  #Convert TOPUP mask nifti to .hdr/.img 
  if [ -f "${output_dir}/T2_mask_RBET_mask_EDIT.img" ]; then 
    fslchfiletype ANALYZE top_up_mask_EDIT.nii.gz #Adding 2/21/19 AG
  fi

  #Build lists of masks and EPIs to run
  top_up_mask_EDIT_list=""
  EPI_list=""
  for f in FWD_EPI*.nii.gz; do
    EPI_list+="${output_dir}/$f "
    if ${run_topup:-false}; then
      #top_up_mask_EDIT_list+="${output_dir}/top_up_mask_EDIT.nii.gz " Dakotas
      top_up_mask_EDIT_list+="${output_dir}/top_up_mask_EDIT.img " # AG changed to .img files instead
    elif ${run_pipeline:-false}; then
      top_up_mask_EDIT_list+="${output_dir}/top_up_mask_EDIT.img "
    fi
  done
  
  ###Run Topup###
  if ${run_topup:-false}; then
    ssub --mem 15G -t 7:00:00 ${wrapper_path}/pipeline_wrapper_FPFSL13_RODENTPREP.py -rmc -s ${sorted_dir} -g ${group:-rt} -etr ${tr:-2.5} -ed ${dwell:-0.256} -tum ${top_up_mask_EDIT_list} \
    -t2m ${output_dir}/T2_mask_RBET_mask_EDIT.nii.gz -en ${EPI_list} -t2n ${output_dir}/T2_late2epi_d_bc.nii.gz -rn ${output_dir}/REV_EPI.nii.gz
  ###Run Pseudo Topup###
  elif ${pseudo_topup:-false}; then
    #Run Topup
    ssub --mem 15G -t 7:00:00 ${wrapper_path}/pipeline_wrapper_FPFSL13_RODENTPREP.py -rmc -s ${sorted_dir} -g ${group:-rt} -etr ${tr:-2.5} -ed ${dwell:-0.256} -tum ${top_up_mask_EDIT_list} \
    -t2m ${output_dir}/T2_mask_RBET_mask_EDIT.nii.gz -en ${EPI_list} -t2n ${output_dir}/T2_late2epi_d_bc.nii.gz -rn ${output_dir}/REV_EPI.nii.gz
    
    #Replace topup_applied_chpixdim.nii.gz with FWD_EPIs
    pushd ${output_dir}/FAIRPRE_FSL_13_RAT_TOPUP_* > /dev/null
    index=1
    for topup in TOPUP_2_FUNC*; do
      pushd ${topup}/8nFUNC_chpx
      cp -f ${output_dir}/FWD_EPI${index}.nii.gz ./topup_applied_chpixdim.nii.gz
      index=$((index+1))
      popd > /dev/null
    done
    popd > /dev/null
  ###Run DFM Pipeline###
  elif ${run_dfm:-false}; then
    #Use the corrected phase nifti, if it exists
    if [ -f "${output_dir}/uphase_rad.nii.gz" ]; then
      corrected_phase="uphase_rad.nii.gz"
    fi
    #Run DFM Pipeline
    ssub --mem 15G -t 7:00:00 ${wrapper_path}/pipeline_wrapper_FAIRPRE_FSL_13.py -rmc -s ${sorted_dir} -g ${group:-rt} -etr ${tr:-2.5} -ed ${dwell:-0.256} -tum ${top_up_mask_EDIT_list} \
    -t2m ${output_dir}/T2_mask_RBET_mask_EDIT.nii.gz -en ${EPI_list} -t2n ${output_dir}/T2_late2epi_d_bc.nii.gz -mn ${output_dir}/MAG.nii.gz -pn ${output_dir}/${corrected_phase:-PHASE.nii.gz} \
    -dm ${output_dir}/MAG_mask_EDIT.nii.gz
  ###Run Pipeline###
  elif ${run_pipeline:-false}; then
    #Clean up directories in preparation to running the pipeline
    pushd ${output_dir}/FAIRPRE_FSL_13_RAT_TOPUP_* > /dev/null
    rm -rf EXTERNAL* FCON* FUNC* MERGED* MK* REG*
    popd > /dev/null
    
    #Run Pipeline after Topup
    ssub --mem 15G -t 7:00:00 ${wrapper_path}/pipeline_wrapper_FAIRPRE_FSL_13.py -rmc -s ${sorted_dir} -g ${group:-rt} -etr ${tr:-2.5} -ed ${dwell:-0.256} -tum ${top_up_mask_EDIT_list} \
    -t2m ${output_dir}/T2_mask_RBET_mask_EDIT.nii.gz -en ${EPI_list} -t2n ${output_dir}/T2_late2epi_d_bc.nii.gz -rn ${output_dir}/REV_EPI.nii.gz
  fi
  popd > /dev/null
fi

#Run Seed Correlations
if ${run_correl:-false}; then
  #Set to DFM directory
  if ${run_dfm:-false}; then
    dfm_dir="FAIRPRE_FSL_13_RAT_NOUNWARP_*"
  fi
  #Run seed correl
  ssub --mem 15G -t 7:00:00 ${wrapper_path}/pipeline_wrapper_SEEDCORREL_3_hpf.py -r ${output_dir}/../../../../ \
  -p ${output_dir}/${dfm_dir:-FAIRPRE_FSL_13_RAT_TOPUP_*}/MK_SDCR_CONC_1 \
  -c ${output_dir}/${dfm_dir:-FAIRPRE_FSL_13_RAT_TOPUP_*}/MK_SDCR_CONC_1/1nCNC4_fcon1/faln_dbnd_xr3d_atl.conc \
  -j ${roi_set_json:-"/group_shares/fnl/bulk/code/external/ROI_sets/Atlas_based/Rat/SHWARZ_FUNC.json"} -tr ${tr:-2.5}
fi