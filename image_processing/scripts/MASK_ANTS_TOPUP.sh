#!/bin/bash -e
#Update History
# 4/11/2016 BDM
# 12/18/17 AAG


if [ $# -ne 1 ]; then
	echo -e "\nUsage:	`basename $0` <subject_directory>"
	exit 1
fi

dir=$1
#sub=$2
#type=$3
#atl=`readlink -f $3`
#atl_brain=`readlink -f $4`

pushd $dir


if [ -f top_up_mask.nii.gz ] ; then
	echo "File top_up_mask exists."
else
	echo "File top_up_mask does not exist."
	#fslsplit ./topup_applied_chpixdim.nii.gz 1 #does this even work??
	flirt -in ./topup_applied_for_mask.nii.gz -ref `dirname ${0}`/ANTS_FILES/TOP_UP.nii.gz -omat FLIRT.mat -out FLIRT
fi

cp FLIRT.nii.gz rot2atl.nii.gz

ANTS 3 -m  CC[rot2atl.nii.gz,`dirname ${0}`/ANTS_FILES/TOP_UP.nii.gz,1,5] -t SyN[0.25] -r Gauss[.5,0] -o atl2T1rot -i 60x50x20 --use-Histogram-Matching  --number-of-affine-iterations 10000x10000x10000x10000x10000 --MI-option 32x16000
#Gauss was 3- changed to .5

antsApplyTransforms -d 3 -i `dirname ${0}`/ANTS_FILES/TOP_UP_mask.nii.gz -t atl2T1rotWarp.nii.gz atl2T1rotAffine.txt -r rot2atl.nii.gz -o rot2atl_mask.nii.gz
convert_xfm -omat orig_mat.mat -inverse FLIRT.mat
flirt -in rot2atl_mask -ref ./topup_applied_for_mask.nii.gz -o top_up_weight -applyxfm -init orig_mat.mat #-interp nearestneighbour

fslmaths top_up_weight -thr .5 -bin top_up_mask

mv top_up_mask.nii.gz ../

rm -f rot*
rm -f atl2*
rm -f FLIRT*
rm -f orig_mat*
rm -f top_up_weight.nii.gz

popd




#fslmaths ${sub} -mas ${sub}_mask ${sub}_brain

#ANTS 3 -m CC['/group_shares/PSYCH/atlases/Rat/EVA_TRANSFORM/ATLAS/EVA_TRANSFORM.nii.gz',FLIRT,1,5] -t SyN[0.25] -r Gauss[.5,0] -o 'ANTS' -i 50x90x30 --use-Histogram-Matching  --number-of-affine-iterations 10000x10000x10000x10000x10000 --MI-option 32x16000

#ANTS 3 -m  CC[FLIRT.nii.gz,/group_shares/fnl/bulk/projects/RODENT/Rat_misc/RAT_MASKING/ANTS_FILES/T2_late2epi_ref.nii.gz,1,5] -t SyN[0.25] -r Gauss[.5,0] -o atl2T1rot -i 60x50x20 --use-Histogram-Matching  --number-of-affine-iterations 10000x10000x10000x10000x10000 --MI-option 32x16000



## NOTE: antsApplyTransforms is deprecated in ANTS 2.0 and might not be available; if so, use WarpImageMultiTransform instead
#back it into native space; threshold it; apply it
#convert_xfm -omat rot2native.mat -inverse rot2atl.mat; flirt -in rot2atl_mask -ref ${sub} -o ${sub}_mask -applyxfm -init ${sub}_rot2native.mat
#fslmaths ${sub}_mask -thr .5 -bin ${sub}_mask
#fslmaths ${sub} -mas ${sub}_mask ${sub}_brain

