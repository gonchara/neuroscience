function auto_spm12_pre(mrid,direct1,recov_ratio,method,grp)

%%%% recov_ratio = from 0-1; ratio of "recovery period following stimulation" to include in Sbaseline. ex: recov_ratio = 0.4, recov period = 20 reps, Sbaseline is the 8 reps directly preceding next stimulation
%%%% Preprocesses (specifically motion corrects the data followed by removing linear
%%%% drift)

%% Defaults

ff = filesep;

%% Define Folders

direct2=strcat(direct1,ff,'PreProcess'); 
if exist(direct2,'dir')==0
    mkdir(direct2);
end

if strcmp('ktfklt',method) || strcmp('ktf',method)
    direct3=strcat(direct1,ff,'CS_',method);
else
    direct3=direct1;
end

%% fMRI
for mm=1:length(mrid)
    
%%% Load parameters for fMRI data      
    tmpname=[direct1,ff,num2str(mrid(:,mm))];   
    disp(mrid(:,mm));
    
    f.sz_mult=100;
    f.tr = getACQP('ACQ_repetition_time', [tmpname,ff,'acqp'])/1000;     % tr (in s)
    f.te  = getACQP('ACQ_echo_time', [tmpname,ff,'acqp']);     % te (in ms)
    f.fov = getACQP('ACQ_fov', [tmpname,ff,'acqp'])*10; % fov in mm
    f.thk = getACQP('ACQ_slice_thick', [tmpname,ff,'acqp']);
    f.time = getACQP('time_stamp', [tmpname,ff,'acqp']);
    
    f.rg = getACQP('RG', [tmpname,ff,'acqp']);    
    rg_check(mm)=f.rg;
    
    if strcmp('ktfklt',method) || strcmp('ktf',method)        
        f.redf  = getMETHOD( 'Red_Factor', [tmpname,ff,'method']);        
        matrix = getACQP('ACQ_size', [tmpname,ff,'acqp']);
        bug=1; if matrix(1)==128, bug=2; end
        f.np = matrix(1)/bug; f.nv = matrix(2).*f.redf;
        f.tr= matrix(2)*f.tr;        
        f.fov(2)=f.fov(2)*f.redf;
        f.nseg=0;
        
    elseif strcmp(method,'epi')
        % matrix =  getMETHOD( 'PVM_EpiMatrix', [tmpname,ff,'method']);
        % Changed this to be able to get matrix dimensions another way. MA
        matrix =  getMETHOD( 'PVM_Matrix', [tmpname,ff,'method']);
        
        f.np = matrix(1); f.nv = matrix(2);
        f.nseg =  getMETHOD( 'NSegments', [tmpname,ff,'method']);% number of EPI segments 
        f.tr = f.tr*f.nseg; % time between time points
        
    elseif strcmp(method,'2DG')
        matrix =  getMETHOD( 'PVM_Matrix', [tmpname,ff,'method']);
        f.np = matrix(1); f.nv = matrix(2);
        f.tsl = getMETHOD( 'TSL', [tmpname,ff,'method']);
        
    end    
    
    f.ns  = getACQP('NSLICES', [tmpname,ff,'acqp']);
    f.ns_offset  = getACQP('ACQ_slice_offset', [tmpname,ff,'acqp']);
    f.nt  = getACQP('NR', [tmpname,ff,'acqp']);
        
    f.pos  = getACQP('ACQ_patient_pos', [tmpname,ff,'acqp']);     % FOV orientation
    f.ns_orient =  getMETHOD( 'PVM_SPackArrSliceOrient', [tmpname,ff,'method']);% number of EPI segments 
    f.read_dir =  getMETHOD( 'PVM_SPackArrReadOrient', [tmpname,ff,'method']);% number of EPI segments 
           
    
    sli_val1 = getMETHOD( 'Sli_val', [tmpname,ff,'method']);% sli_val = # trials  
    ind=find(sli_val1>=1);    
    f.sli=zeros(1,length(sli_val1));
    f.sli(ind)=1;    
    f.sli_check=max(sli_val1(:));

    if isempty(f.sli_check) || f.sli_check==0 
        f.Sbaseline=1:f.nt;
    else
        dsli=diff(f.sli);
        indP=find(dsli==1);
        indN=find(dsli==-1);
        
        % Find baseline period before 1st stimulation
        base=indP(1);
        
        % Calculate Sbaseline
        f.Sbaseline=1:base;
        for nn=1:length(indP)
            if nn==length(indP)
                en=length(f.sli);
            else
                en=indP(nn+1);
            end
            
            recov(nn)=en-indN(nn);
            Stemp=en-roundn(recov_ratio*recov(nn),0);
            Stemp=Stemp+1:en;
            
            f.Sbaseline=cat(2,f.Sbaseline,Stemp);
        end
    end

        
    %% Load Functional Images
    
    if strcmp('ktfklt',method) || strcmp('ktf',method)
        fname=strcat(direct3,ff,'flash_cs_',sprintf('%02d',mrid(mm)),'_',method);
        raw_fmri=rdSdt(fname);
        
    else
        fid=fopen([tmpname,ff,'pdata',ff,'1',ff,'2dseq']);
        
        raw_fmri=fread(fid,[1 f.np*f.nv*f.ns*f.nt],'uint16'); %uncommented for PITT data AG
        % int32 for OHSU opto rats. MA
        %raw_fmri=fread(fid,[1 f.np*f.nv*f.ns*f.nt],'uint32');
        
        raw_fmri=reshape(raw_fmri,f.np,f.nv,f.ns,f.nt);
        %raw_fmri=reshape(raw_fmri,f.np,f.ns,f.nv,f.nt);
        raw_fmri=permute(raw_fmri,[2,1,3,4]);
        
        if strcmp(f.pos,'Head_Supine') && strcmp(f.ns_orient,'axial')
                raw_fmri=flipdim(raw_fmri,3);
        else
                error('Update Reorientation code here')
        end
    end        
%     figure; imagesc(reshape(raw_fmri(:,:,:,50),f.np,f.nv*f.ns)); axis image; colormap gray
    
    
    raw.mean=mean(raw_fmri(:,:,:,f.Sbaseline),4);
%     figure; imagesc(reshape(raw.mean,f.np,f.nv*f.ns)); axis image; colormap gray


    svfname=strcat(direct2,ff,sprintf('%02d',mrid(:,mm)),'.mat');
    
    %%% Quality control
    fqc=strcat(direct2,ff,'QC_sli',num2str(f.sli_check),'.mat');
    if exist(fqc,'file')==2
        load(fqc);
        
        qc=table(f.tr,f.te,f.fov(1),f.fov(2),f.thk,f.rg,f.np,f.nv,f.nseg,f.ns,f.nt,cellstr(svfname),'VariableNames',{'TR','TE','FOV1','FOV2','thk','rg','np','nv','nseg','ns','nt','fname'});
        temp=isequaln(Tqc(1,1:end-1),qc(:,1:end-1));
        temp2=table2cell(Tqc(:,end)); temp2=cell2mat(strfind(temp2,svfname));
        if temp==0 && isempty(temp2)
            Tqc=cat(1,Tqc,qc);
            save(fqc,'Tqc');
        end        
    else
        Tqc=table(f.tr,f.te,f.fov(1),f.fov(2),f.thk,f.rg,f.np,f.nv,f.nseg,f.ns,f.nt,cellstr(svfname),'VariableNames',{'TR','TE','FOV1','FOV2','thk','rg','np','nv','nseg','ns','nt','fname'});
        save(fqc,'Tqc');
    end
    

    if exist(svfname,'file')==2
        save(svfname,'grp','f','raw','raw_fmri','-append');
    else
        save(svfname,'grp','f','raw','raw_fmri');
    end

    fclose('all');
    clear f raw raw_fmri tmpname temp temp2 qc Tqc
end

