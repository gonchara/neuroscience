#! /bin/bash


# example command to call this script:
# /home/exacloud/tempwork/fnl_lab/code/internal/pipelines/opto-fMRI/opto-fMRI_pipeline.sh -o /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/output/PIKE02112019_RAT4303_ELEC_STIM/20190211 -s PIKE02112019_RAT4303_ELEC_STIM -b /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/input/PIKE02112019_RAT4303_ELEC_STIM.RN1 -r /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/input/PIKE02112019_RAT4303_ELEC_STIM_REVERSE.RN1 -n 5 -t 10 -e "12 13 14 15 16 17 18 19 20 21 22 23"
#
#
#
#

TEMP=`getopt -o ho:s:b:t:e:r:n:d:fuwcj:g:z:x:mp --long help,output:,subject:,brukerdata:,t2:,epi:,revepi:,revnum:,dfm:,rundfm,topup,pseudo,correl,roijson:,group:,dwell:,tr:,masktopup,pipeline \
-n 'test_bash.sh' -- "$@"`
eval set -- "$TEMP"

pseudo_topup=false

while true ; do
  case ${1} in
    -h|--help) echo -e "\nUsage:\n\n Required Args:\n"
        echo -e "\t-o,--output [<output directory> Path to subject directory. (Ex. .../processed/scan_type/subject/date)]"
        echo -e "\t-s,--subject [<subject id> Subject ID (Ex. OptoRat_M314_20180420).]"
        echo -e "\t-h,--help [usage help]"
        echo -e "\t-b,--brukerdata [<path to bruker data directory> Pulls and converts bruker data to nifti format.]"
        echo -e "\t-t,--t2 [<T2w folder number> Pull T2w to output directory then Denoise and BiasField Correct.]"
        echo -e "\t-e,--epi [<List of EPI folder numbers as a string (Ex. \"10 11 12\")> Pull EPIs to output directory.]"
        echo -e "\t-r,--revepi [<Reverse EPI bruker path> Convert REV_EPI bruker data to niftis.]"
        echo -e "\t-n,--revnum [<Reverse EPI folder number> Pull REV_EPI to output directory.]"
        echo -e "\t-x,--tr [EPI Repetition Time in seconds.]"
	echo -e "\t-g,--group [species name to determine which atlas to use, can be 'mouse' or 'rat']"
	echo -e "\t-w,--psuedo [skips all distortion correction via reverse-phase encoding EPIs]"
        exit 1 ;;
    -o|--output) OUTPUT_DIR=$2 ; shift 2 ;;
    -s|--subject) SUBJECT_ID=$2 ; shift 2 ;;
    -b|--brukerdata) MAIN_BRUKER_DIR=$2 ; shift 2 ;;
    -t|--t2) T2_SERIES_NUMBER=$2 ; shift 2 ;;
    -e|--epi) EPI_SERIES_NUMBERS=($2) ; shift 2 ;;
    -r|--revepi) REV_EPI_BRUKER_DIR=$2; shift 2 ;;
    -n|--revnum) REV_EPI_SERIES_NUMBER=$2; shift 2 ;;
    -d|--dfm) dfm_number=$2; shift 2 ;;
    -f|--rundfm) run_dfm=true; shift ;;
    -u|--topup) run_topup=true; shift ;;
    -w|--pseudo) pseudo_topup=true; shift ;;
    -c|--correl) run_correl=true; shift ;;
    -j|--roijson) roi_set_json=$2; shift 2 ;;
    -g|--group) group=$2; shift 2 ;;
    -z|--dwell) dwell=$2; shift 2 ;;
    -x|--tr) TR=$2; shift 2 ;;
    -m|--masktopup) mask_topup=true; shift ;;
    -p|--pipeline) run_pipeline=true; shift ;;
    --) shift ; break ;;
    *) echo "ERROR: Unrecognized Option" ; exit 1 ;;
  esac
done
####################################
### vvv DEFAULT VARIABLES!!! vvv ###
####################################

#set group by default to rat -- so rat atlas is used by default
group=${group:-'rat'}

####################################
### vvv HARDCODED WARNING!!! vvv ###
####################################

# Dakota's simplified rodent utility script
DAKOTA_SCRIPT=`dirname ${0}`/run_rodent_subj.sh

# Brian's simplified ANTS T2 masking script
MASK_T2_SCRIPT=`dirname ${0}`/MASK_ANTS_T2_d_bc.sh

# Brian's simplified ANTS TOPUP masking script
MASK_TOPUP_SCRIPT=`dirname ${0}`/MASK_ANTS_TOPUP.sh

# David Grayson's rodent-tailored motion correction script
MOCO_SCRIPT=`dirname ${0}`/motion_correction.sh

# Frame count of FWD and REV EPI's to use in fsltopup
TOPUP_FRAME_COUNT=15

# T2 atlas in use
if [ $group == mouse ]; then T2_ATLAS_BRAIN=/home/exacloud/tempwork/fnl_lab/atlases/mouse/c57_11/c57_11_M_Normal_age12_200200200.nii.gz; fi
if [ $group == rat ]; then T2_ATLAS_BRAIN=/home/exacloud/tempwork/fnl_lab/atlases/rat/EVA_TRANSFORM/ATLAS/EVA_TRANSFORM.nii.gz; fi
#T2_ATLAS_BRAIN=/group_shares/fnl/bulk/atlases/Rat/EVA_TRANSFORM/ATLAS/EVA_TRANSFORM.nii.gz
#mouse atlas: /home/exacloud/tempwork/fnl_lab/atlases/mouse/c57_11/c57_11_M_Normal_age12_200200200.nii.gz

# post-topup mask expected name
EPI_MASK=${OUTPUT_DIR}/top_up_mask_EDIT.img

# T2 mask expected name
T2_MASK=${OUTPUT_DIR}/T2_mask_RBET_mask_EDIT.nii.gz

# Pipeline name
PIPELINE_NAME="opto-fMRI_pipeline"

# Name for the EPI Scout image
SCOUT="EPI_scout"

####################################
### ^^^ HARDCODED WARNING!!! ^^^ ###
####################################


#PREP DATA with bruker2nifti
if [ ! -d ${OUTPUT_DIR}/b2n_data -a ! -d ${OUTPUT_DIR}/b2n_rev_data ] ; then
    echo "Converting BRUKER files into NIfTI files"
    if [ ${pseudo_topup} == false ]; then
    	${DAKOTA_SCRIPT} -o ${OUTPUT_DIR} -s ${SUBJECT_ID} -b ${MAIN_BRUKER_DIR} -r ${REV_EPI_BRUKER_DIR}
    else
    	${DAKOTA_SCRIPT} -o ${OUTPUT_DIR} -s ${SUBJECT_ID} -b ${MAIN_BRUKER_DIR}
    fi	
fi

if [ ! -f ${OUTPUT_DIR}/T2_late2epi_d_bc.nii.gz ] ; then
    ${DAKOTA_SCRIPT} -o ${OUTPUT_DIR} -s ${SUBJECT_ID} --t2 ${T2_SERIES_NUMBER}
fi

if [ ! -f ${OUTPUT_DIR}/FWD_EPI1.nii.gz ] ; then
    index=1
    for i in "${EPI_SERIES_NUMBERS[@]}"; do
        echo "Copying EPI from series: ${i}"
        cp -f ${OUTPUT_DIR}/b2n_data/b2n_data_${i}/b2n_data_${i}.nii.gz ${OUTPUT_DIR}/FWD_EPI${index}.nii.gz
        index=$(($index+1))
     done
fi

if [ ! -f ${OUTPUT_DIR}/REV_EPI.nii.gz ] ; then
    if [ ${pseudo_topup} == false ]; then
        ${DAKOTA_SCRIPT} -o ${OUTPUT_DIR} -s ${SUBJECT_ID} --revnum ${REV_EPI_SERIES_NUMBER}
    fi
fi

pushd ${OUTPUT_DIR}

if [ ! -d ${PIPELINE_NAME} ] ; then
    mkdir ${PIPELINE_NAME}
fi

pushd ${PIPELINE_NAME}
# RUN TOPUP
if [ ! -f topup_fieldcoef.nii.gz -a ! -f topup_movpar.txt ] || [ ${pseudo_topup} = true ]; then
    if [ ${pseudo_topup} == false ]; then
    	echo "Running TOPUP"
    	fslroi ../FWD_EPI1.nii.gz FWD_EPI_roi.nii.gz 0 ${TOPUP_FRAME_COUNT}
	fslroi ../REV_EPI.nii.gz REV_EPI_roi.nii.gz 0 ${TOPUP_FRAME_COUNT}

        fslmaths FWD_EPI_roi.nii.gz -Tmean FWD_EPI_roi_mean.nii.gz
        fslmaths REV_EPI_roi.nii.gz -Tmean REV_EPI_roi_mean.nii.gz
        fslswapdim REV_EPI_roi_mean.nii.gz x -y z REV_EPI_roi_mean_swapY.nii.gz
        flirt -dof 6 -interp spline -in REV_EPI_roi_mean_swapY.nii.gz -ref FWD_EPI_roi_mean.nii.gz -out REV_to_FWD.nii.gz
        fslmerge -t topup_EPIs.nii.gz FWD_EPI_roi_mean.nii.gz REV_to_FWD.nii.gz

        # MAKE TOPUP SCAN PARAMETERS FILE
        rm -f topup_scan_param*.txt
        touch topup_scan_param_X_phase_encode.txt
        touch topup_scan_param_Y_phase_encode.txt
        touch topup_scan_param_Z_phase_encode.txt

        #for FWD in `seq  ${TOPUP_FRAME_COUNT}` ; do
            echo "1 0 0 1" >> topup_scan_param_X_phase_encode.txt # Z: left<->right forward phase encode
            echo "0 1 0 1" >> topup_scan_param_Y_phase_encode.txt # Y: inferior<->superior forward phase encode
            echo "0 0 1 1" >> topup_scan_param_Z_phase_encode.txt # X: anterior<->posterior forward phase encode
        #done

        #for REV in `seq  ${TOPUP_FRAME_COUNT}` ; do
            echo "-1 0 0 1" >> topup_scan_param_X_phase_encode.txt # Z: left<->right reverse phase encode
            echo "0 -1 0 1" >> topup_scan_param_Y_phase_encode.txt # Y: inferior<->superior reverse phase encode
            echo "0 0 -1 1" >> topup_scan_param_Z_phase_encode.txt # X: anterior<->posterior reverse phase encode
        #done

        # CHOOSING PHASE ENCODING DIRECTION
        cp -f topup_scan_param_Y_phase_encode.txt topup_scan_param.txt
  
        srun --mem 8G -t 04:00:00 topup --out=topup --imain=topup_EPIs.nii.gz --datain=topup_scan_param.txt

        #topup --out=topup --imain=topup_EPIs.nii.gz --datain=topup_scan_param.txt
        #rm -f topup_EPIs.topup_log
        #topup --out=topup_X --imain=topup_EPIs.nii.gz --datain=topup_scan_param_X_phase_encode.txt
        #rm -f topup_EPIs.topup_log
        #topup --out=topup_Y --imain=topup_EPIs.nii.gz --datain=topup_scan_param_Y_phase_encode.txt
        #rm -f topup_EPIs.topup_log
        #topup --out=topup_Z --imain=topup_EPIs.nii.gz --datain=topup_scan_param_Z_phase_encode.txt
        #rm -f topup_EPIs.topup_log

        srun --mem 8G -t 03:00:00 applytopup --out=topup_applied_for_mask --imain=../FWD_EPI1.nii.gz --inindex=1 --datain=topup_scan_param.txt --topup=topup --method=jac #Testing for segmentation fault errors AG
        #applytopup --out=topup_applied --imain=../FWD_EPI1.nii.gz --inindex=1 --datain=topup_scan_param.txt --topup=topup --method=jac #removed "fsl5.0" AG
        #fsl5.0-applytopup --out=topup_applied --imain=../FWD_EPI1.nii.gz --inindex=1 --datain=topup_scan_param.txt --topup=topup --method=jac #commented out to test above  AG
        #fsl5.0-applytopup --out=topup_applied_X --imain=../FWD_EPI1.nii.gz --inindex=1 --datain=topup_scan_param_X_phase_encode.txt --topup=topup --method=jac
        #fsl5.0-applytopup --out=topup_applied_Y --imain=../FWD_EPI1.nii.gz --inindex=1 --datain=topup_scan_param_Y_phase_encode.txt --topup=topup --method=jac
        #fsl5.0-applytopup --out=topup_applied_Z --imain=../FWD_EPI1.nii.gz --inindex=1 --datain=topup_scan_param_Z_phase_encode.txt --topup=topup --method=jac
    else
	cp ../FWD_EPI1.nii.gz topup_applied_for_mask.nii.gz #if psuedo-topup is enabled, just copy the FWD EPI for mask generation
    fi
fi

# CHECK FOR EXISTENCE OF MASKS BEFORE CONTINUING
if [ ! -f ${EPI_MASK} ] ; then
    echo "No post-topup mask edited yet here: ${EPI_MASK}"
fi
#CREATE TOPUP MASK
if [ ! -f ../top_up_mask.nii.gz ] ; then
    echo "Creating preliminary TOPUP mask"
    srun --mem 8G -t 07:00:00 ${MASK_TOPUP_SCRIPT} .
fi

if [ ! -f ${T2_MASK} ] ; then
    echo "No T2 mask edited yet here: ${T2_MASK}"
fi
#CREATE T2 MASK
if [ ! -f ../T2_mask_RBET_mask.nii.gz ] ; then
    echo "Creating preliminary T2 mask"
    srun --mem 8G -t 07:00:00 ${MASK_T2_SCRIPT} . ${group}
fi

# LOOP OVER FWD_EPI'S

if [ -f ${EPI_MASK} -a -f ${T2_MASK} ] ; then

    for EPI_FULLNAME in ../FWD_EPI*.nii.gz ; do
        # MAKE SOME SHORT NAMES FOR FILES
        EPI=`basename ${EPI_FULLNAME}`
        EPI_SHORTNAME=`echo ${EPI} | sed 's|\.nii\.gz$||g' | sed 's|\.hdr$||g' | sed 's|\.img$||g'`
        MC_EPI=${EPI_SHORTNAME}_masked_mcf.nii.gz

        fslorient -setsformcode 1 ${EPI_FULLNAME}
        fslorient -forceneurological ${EPI_FULLNAME}

        if [ ! -f ${MC_EPI} ] ; then
            # MOTION CORRECTION WITHIN RUN
            echo "Motion correction on ${EPI_SHORTNAME}..."
            cp -f ${EPI_FULLNAME} ${EPI}
            ${MOCO_SCRIPT} ${EPI} ${EPI_MASK}
        fi

        if [ ! -f topup_applied.nii.gz -a -f ${MC_EPI} ] || [ ${pseudo_topup} = true ]; then
	    if [ ${pseudo_topup} == false ]; then
            	# APPLYING THE TOPUP DISTORTION CORRECTION
	        echo "APPLYTOPUP on ${EPI_SHORTNAME}..."
            	applytopup --out=topup_applied --imain=${MC_EPI} --inindex=1 --datain=topup_scan_param.txt --topup=topup --method=jac
            	#fsl5.0-applytopup --out=topup_applied_X --imain=${MC_EPI} --inindex=1 --datain=topup_scan_param_X_phase_encode.txt --topup=topup_X --method=jac
            	#fsl5.0-applytopup --out=topup_applied_Y --imain=${MC_EPI} --inindex=1 --datain=topup_scan_param_Y_phase_encode.txt --topup=topup_Y --method=jac
            	#fsl5.0-applytopup --out=topup_applied_Z --imain=${MC_EPI} --inindex=1 --datain=topup_scan_param_Z_phase_encode.txt --topup=topup_Z --method=jac

            	# MASK EPI
            	fslmaths topup_applied.nii.gz -mas ${EPI_MASK} ${EPI_SHORTNAME}_final.nii.gz
            	#fslmaths topup_applied_X.nii.gz -mas ${EPI_MASK} ${EPI_SHORTNAME}_X_final.nii.gz
            	#fslmaths topup_applied_Y.nii.gz -mas ${EPI_MASK} ${EPI_SHORTNAME}_Y_final.nii.gz
            	#fslmaths topup_applied_Z.nii.gz -mas ${EPI_MASK} ${EPI_SHORTNAME}_Z_final.nii.gz

            	#cp ${EPI_SHORTNAME}_Y_final.nii.gz ${EPI_SHORTNAME}_final.nii.gz
	    else
		fslmaths ${MC_EPI} -mas ${EPI_MASK} ${EPI_SHORTNAME}_final.nii.gz
            fi
        fi

        # FIX TR IN FINAL EPI
        PIXDIM1_ORIG=`fslinfo ${EPI_SHORTNAME}_final.nii.gz | grep pixdim1 | awk '{print $2}'`
        PIXDIM2_ORIG=`fslinfo ${EPI_SHORTNAME}_final.nii.gz | grep pixdim2 | awk '{print $2}'`
        PIXDIM3_ORIG=`fslinfo ${EPI_SHORTNAME}_final.nii.gz | grep pixdim3 | awk '{print $2}'`
        fslchpixdim ${EPI_SHORTNAME}_final.nii.gz ${PIXDIM1_ORIG} ${PIXDIM2_ORIG} ${PIXDIM3_ORIG} ${TR}

        echo "forcing back into neurological orientation (fslchpixdim resets to radiological)"
        fslorient -setsformcode 1 ${EPI_SHORTNAME}_final.nii.gz
        fslorient -forceneurological ${EPI_SHORTNAME}_final.nii.gz
	if [ ${pseudo_topup} == false ]; then
            rm -rf topup_applied*.nii.gz ${EPI} ${EPI_SHORTNAME}_masked* ${EPI_SHORTNAME}_scaleX*
        else
            rm -rf ${EPI} ${EPI_SHORTNAME}_masked* ${EPI_SHORTNAME}_scaleX*
        fi
    done

    # MASK T2
    fslmaths ../T2_late2epi_d_bc.nii.gz -mas ${T2_MASK} T2_masked.nii.gz

    # REGISTER FROM EPI TO ATLAS AND GENERATE TRANSFORM
    fslmaths FWD_EPI1_final.nii.gz -Tmean ${SCOUT}.nii.gz
    flirt -dof 6 -cost mutualinfo -in ${SCOUT}.nii.gz -ref T2_masked.nii.gz -out ${SCOUT}_to_T2.nii.gz -omat ${SCOUT}_to_T2.mat -searchrx -30 30 -searchry -30 30 -searchrz -30 30
    flirt -in T2_masked.nii.gz -ref ${T2_ATLAS_BRAIN} -out T2_to_ATLAS.nii.gz -omat T2_to_ATLAS.mat
    convert_xfm -omat ${SCOUT}_to_ATLAS.mat -concat T2_to_ATLAS.mat ${SCOUT}_to_T2.mat

    for EPI in FWD_EPI*_final.nii.gz ; do
        # MAKE SOME SHORT NAMES FOR FILES
        EPI_SHORTNAME=`echo ${EPI} | sed 's|\.nii\.gz$||g' | sed 's|\.hdr$||g' | sed 's|\.img$||g'`
        AVERAGED_EPI=${EPI_SHORTNAME}_Tmean
        REGISTERED_EPI=${AVERAGED_EPI}_to_${SCOUT}

        fslmaths ${EPI} -Tmean ${AVERAGED_EPI}.nii.gz
        flirt -dof 6 -in ${AVERAGED_EPI}.nii.gz -ref ${SCOUT}.nii.gz -out ${REGISTERED_EPI}.nii.gz -omat ${REGISTERED_EPI}.mat

        convert_xfm -omat ${EPI_SHORTNAME}_to_ATLAS.mat -concat ${SCOUT}_to_ATLAS.mat ${REGISTERED_EPI}.mat

        rm -f ${AVERAGED_EPI}*.nii.gz
    done
else
    echo "Skipping all image registrations (within run, between run, and to atlas) due to missing masks"
fi

popd
popd

