%% This is a Utility to make niftis from Alex's outputs
clear all; close all; clc;
addpath('/mnt/max/shared/code/external/utilities/Matlab_nifti_and_ANALYZE');

ff = filesep;

% Path to subject directory 
sdir = '/mnt/max/shared/data/rodent/opto-fmri/input';
sub_id = 'PIKE07172019_MOUSE_OPTO_ELECTSTIM_7RT.Uj1';

%path = '/mnt/max/shared/projects/OPTO_FMRI/alex_poplawsky_analysis/OptoRat_M212_20180606.NK1/PreProcess/';
path = [sdir ff sub_id];
% List the epi sequence numbers with the light stimulus 
%seq = [11:15,17];
seq = [19:31];%[20:23,25:33];% [13]%[15:22]; %
% Concatenated glm output
concat_seq = {[19:26],[19:23],[24:26],[27:31]};%{[19:20],[21:22],[19:22]}; %{[9:10],[11:12],[9:12],[13:15]}; %same as your mrid_stim in start_here.m

%% make niftis in PreProcess folder
for i=1:length(seq)
    load([path ff 'PreProcess' ff num2str(seq(i),'%02d') '.mat']);
    
    mc_path = [path ff 'PreProcess' ff num2str(seq(i),'%02d') '_mc_fmri.nii']
    tmap_path = [path ff 'PreProcess' ff num2str(seq(i),'%02d') '_tmap.nii'];
    
    if ~exist(mc_path,'file')
        mc_nii = make_nii(mc_fmri);
        save_nii(mc_nii, mc_path); 
    end
    
    % This wont work for non-stim epis or concatenated glms - this is for
    % non cat glm
    %if ~exist(tmap_path,'file')
    %    tmap_nii = make_nii(glm.tmap); %commented out AG
    %    save_nii(tmap_nii,tmap_path);%comemented out AG
    %end
end
%% Make niftis for the concatenated glms
for i = 1:length(concat_seq)
    % auto-naming the concatenated stimulus file outputs from the following function
    concat_filename = strrep(num2str(concat_seq{i}),' ', '_'); % Epi sequences w/ light stim
    tmap_path = [path ff concat_filename '_tmap.nii']
    % This concatonates all blocks from all sequences defined in mrid_stim for glm.
    load([path ff 'cat_BOLD_' concat_filename]);
    tmap_nii = make_nii(glm.tmap);
    save_nii(tmap_nii,tmap_path);
end