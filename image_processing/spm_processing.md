# Optogenetrics-fMRI Rodent Processing - SPM

The Optogenetics-fMRI project is a collaboration between Damien Fair and Bita Moghaddam for rats and Cris Niell for mice . The current work aims to replicate Bita's previous work at Pitt where they used SPM for processing/analysis. The code for this pipeline has been obtained from Alex Poplawsky and modified by Moosa Ahmed. It is currently on rushmore where it was tested.


Required scripts, pipelines, utilities, images etc.:
- [start_here.m](scripts/start_here.m)
- [getMETHOD.m](scripts/getMETHOD.m)
- [getACQP.m](scripts/getACQP.m)
- [auto_spm12_SPMmotion.m](scripts/auto_spm12_SPMmotion.m)
- [auto_spm12_pre.m](scripts/auto_spm12_pre.m)
- [auto_spm12_glm_runcat.m](scripts/auto_spm12_glm_runcat.m)
- [auto_spm12_glm_run.m](scripts/auto_spm12_glm_run.m)
- [make_niis.m](scripts/make_niis.m)
- [baphy_make_timing_file.m](../misc/utilities/baphy_make_timing_file.m)

>
[Path Directory](https://gitlab.com/Fair_lab/RODENT/blob/master/directory.md):
- Bruker raw data (Original Bruker data)
- SPM Scripts
- Error & output files: *WHERE WILL THESE BE?*
- Scan Notes
- Timing Files
- Timing File Convert Script

## Prep Timing Files

### SCP Timing Files

1. If not done so after acquisition, [SCP](../misc/utilities/scp_info.md) timing files from laptop to @airclogin-a1 
- Login to laptop MACC056 & open WinSCP
- Copy files from (C:)/Data/$ScanName/ to @airclogin-a1:/group_shares/fnl/bulk/projects/RODENT/CYA/processed/Rat_Opto_fMRI/$ScanName/$Date/timing_files/

### Convert Timing Files

1. Convert timing files with `baphy_make_timing_file.m`
- Open MATLAB with interactive session (type `sdev --mem 5G -t 8:00:00` then `matlab` in terminal)
- Open `/group_shares/fnl/bulk/projects/RODENT/Rat_misc/scripts/baphy_make_timing_file.m` 
- In Current Folder window, navigate to `/group_shares/fnl/bulk/projects/RODENT/Rat_misc/`, right click on `scripts`, select Add to Path/Selected Folders and Subfolders.
- In Command Window submit `baphy_make_timing_file` command one at a time until all timing files converted (Example >> `baphy_make_timing_file('/group_shares/fnl/bulk/projects/RODENT/CYA/processed/testrun/OptoRat_M212_20180626/20180626/timing_files/OptoRat_M212_20180626_2018_06_26_NON_1_20I.m'`)

### Rsync Timing Files

1. Log on to @rushmore and confirm directory for timing files exists, if not create it `${USERNAME}@rushmore:/mnt/max/shared/projects/OPTO_FMRI/alex_poplawsky_analysis/${RAT}/${DATE}/`
2. Log on to @airclogin-a1
3. Edit rsync command
Command:
```
rsync -ulrHSE --bwlimit=10240 /group_shares/fnl/bulk/projects/RODENT/CYA/processed/${STUDY}/${SCANNAME}/timing_files/ ${USERNAME}@rushmore:/mnt/max/shared/projects/OPTO_FMRI/alex_poplawsky_analysis/${RAT}/${DATE}/
```

Example:
```
rsync -ulrHSE --bwlimit=10240 /group_shares/fnl/bulk/projects/RODENT/CYA/processed/testrun/OptoRat_M212_20180626/20180626/timing_files/ gonchara@rushmore:/mnt/max/shared/projects/OPTO_FMRI/alex_poplawsky_analysis/OptoRat_M212/20180626/
```

### Move & Rename Timing Files

1. Log on to @rushmore
2. Navigate to the timing files `/mnt/max/shared/projects/OPTO_FMRI/alex_poplawsky_analysis/${RAT}/${DATE}/`
3. Pull up [Scan_Notes](https://ohsu.box.com/s/adiwjxkuslmcu33bvs75m3vjjnb6yb3s) to know which folder each timing file belongs in
4. Move each `.txt` file one by one to correct folder.
5. Once file is in it appropriate folder rename to `timing_file.txt`

## Rsync Raw Data
When MRI scans are acquired with Bruker, they get stored to `/bruker/12T/jarrett/nmr` or `/bruker/12T/mpike/nmr` (every night at 12:00 am). To process with SPM, the bruker scan directory needs to be rsynced to rushmore.

1. Login to @airclogin-a1 and navigate to `/bruker/12T/jarrett/nmr` or `/bruker/12T/mpike/nmr` to get exact path for subject (Example: /bruker/12T/mpike/nmr/OptoRat_M212_20180626.O41)
2. Edit rsync command
Command:
```
rsync -ulrHSE --bwlimit=10240 /bruker/12T/${LOGIN}/nmr/${SCANNAME} ${USERNAME}@rushmore:/mnt/max/shared/projects/OPTO_FMRI/alex_poplawsky_analysis/
```

Example:
```
rsync -ulrHSE --bwlimit=10240 /bruker/12T/mpike/nmr/OptoRat_M212_20180626.O41 gonchara@rushmore:/mnt/max/shared/projects/OPTO_FMRI/alex_poplawsky_analysis/
```


---

MATLAB

start_here.m:
bruker directory
timing files directory
linear detrend yes or no

EPI seq
EPI with stim

prestep - sets up variables

motion correction light or strict
Moosa will document in readme what the specs mean

if timing file is used (Alexs is no)

run
uses each block within stimulus to make general linear model

What we want
concat sequences and then run GLM on all (5blocks per one)
damien has been wanting them separate

-run cat 
- change how you want output


---

view in fsl

make nii
files are in Preprocess as mat files
for concat will go into subject folder.

---

select up to auto_spm12_SPMmotion(mrid,strcat(mdir,sdir),0.2, 24, 10, 4, lin);
right click
Evaluate selection


### Convert to Nifti

Open make_nii.m




# FUTURE UPDATES COMING FOR PROCESSING ON AIRCLOGIN-A1 --- DO NOT USE YET JUN2018

## I. Pull Raw Data & Mask

When MRI scans are acquired with Bruker, they get stored to `/bruker/12T/jarrett/nmr` or `/bruker/12T/mpike/nmr` (every night at 12:00 am). To process with SPM, the bruker scan directory needs to be rsynced to rushmore.

### Pull Raw Data with rsync

Raw data is pulled with [Bruker2nifti](https://github.com/SebastianoF/bruker2nifti/wiki/Graphical-User-Interface-Examples) which is located `@airclogin-a1:/public/code/external/utilities/bruker2nifti/venv/bin`. 

1. Logon to airclogin-a1
	- Use x2goclient to logon
2. Get necessary scan acquisition info from Scan Notes on [BOX/.../Opto-fMRI_Study/data/](https://ohsu.box.com/s/adiwjxkuslmcu33bvs75m3vjjnb6yb3s). *need to create scan notes info page*.
    - Subject ID (`-s`)
    - Raw bruker data path (`-b`)
    - T2 scan number (`-t`)
    - EPI scan numbers (`-e`)
    - REV_EPI  bruker data path (`-r`)
    - REV_EPI scan number (`-n`)
    - Desired or existing output directory (`-o`)
3. Edit command for `run_rodent_subj.sh` with options `-b` `-t` `-e` `-r` `-n` to pull and mask raw data
    - Additional notes `/group_shares/fnl/bulk/projects/RODENT/Rat_misc/Rat_pull_data/run_rodent_subj_README.txt`
Command:

```
/group_shares/fnl/bulk/projects/RODENT/Rat_misc/Rat_pull_data/run_rodent_subj.sh -o [output_directory] -s [subject_id] -b [raw_bruker_data_path] -t [T2w_folder_number] -e [list_of_epi_folder_numbers_as_string] -r [rev_epi_bruker_data_path] -n [rev_epi_folder_number]
```

Example:

```
/group_shares/fnl/bulk/projects/RODENT/Rat_misc/Rat_pull_data/run_rodent_subj.sh -o /group_shares/fnl/bulk/projects/RODENT/CYA/processed/testrun/OptoRat_M209_20180613/20180613 -s OptoRat_M209_20180613 -b /bruker/12T/jarrett/nmr/OptoRat_M209_20180613.NR1 -r /bruker/12T/jarrett/nmr/OptoRat_M209_20180613_rev.NR1 -n 2 -t 5 -e "8 9 10 11 12 13 14 15 16"
```
- Output: b2n_data, b2n_rev_data, FWD_EPIs, REV_EPI, T2_late2epi, T2_late2epi_d, T2_late2epi_d_bc, T2_mask_RBET_mask

### Edit T2 Mask

Automatic masking creates a binary map marking portions of the image that contain the brain. These generated masks require some manual editing which is accomplished in FSLView.

1. Navigate to subject folder (Example: `/group_shares/fnl/bulk/projects/RODENT/CYA/processed/Rat_Opto_fMRI/OptoRat_M314_20180420/20180420/`)
2. Select `T2_late2epi_d_bc.nii.gz` & `T2_mask_RBET_mask.nii.gz`, open in FSLView
3. Edit T2 mask
	- In the bottom right, unclick the lock on the mask file, click blue "i" icon, change "lookup table options" to Red. 
	- When done save as: `T2_mask_RBET_mask_EDIT` (Output: T2_mask_RBET_mask_EDIT.hdr & T2_mask_RBET_mask_EDIT.img)


## II. Topup

### Run Topup

Topup runs the first portion of the pipeline.

1. Edit command for `run_rodent_subj.sh` with option `-u` to run topup
    - Additional notes `/group_shares/fnl/bulk/projects/RODENT/Rat_misc/Rat_pull_data/run_rodent_subj_README.txt`

Command:

```
/group_shares/fnl/bulk/projects/RODENT/Rat_misc/Rat_pull_data/run_rodent_subj.sh -o [output_directory] -s [subject_id] -u
```

Example:
```
/group_shares/fnl/bulk/projects/RODENT/CYA/processed/testrun/OptoRat_M209_20180613/20180613$ /group_shares/fnl/bulk/projects/RODENT/Rat_misc/Rat_pull_data/run_rodent_subj.sh -o /group_shares/fnl/bulk/projects/RODENT/CYA/processed/testrun/OptoRat_M209_20180613/20180613 -s OptoRat_M209_20180613 -u
```

- Output: Generated the `FAIRPRE_FSL_13_RAT_TOPUP_LIN` folder
- Once job is complete you will see a `FAIRPRE_FSL_13_RAT_TOPUP_LIN` folder in the subject's directory. Check the output and error files located at `/tmp/username/slurm_jobs`
3. Check Topup quality `subject folder/FAIRPRE_FSL_13_RAT_TOPUP_LIN/TOPUP_2_FUNC1/8nFUNC_chpx/topup_applied_chpixdim.nii.gz`
- If good move `topup_applied_chpixdim.nii.gz` file to main Subject folder



### Run Pseudo Topup

1. Edit command for `run_rodent_subj.sh` with option  `-w` to run pseudo topup
.../run_rodent_subj.sh -o [output_directory] -s [subject_id] -w



## III. Topup Mask

### Mask Topup
1. Edit command for `run_rodent_subj.sh` with option `-m` to create topup mask
    - Additional notes `/group_shares/fnl/bulk/projects/RODENT/Rat_misc/Rat_pull_data/run_rodent_subj_README.txt`
Command:

```
/group_shares/fnl/bulk/projects/RODENT/Rat_misc/Rat_pull_data/run_rodent_subj.sh -o [output_directory] -s [subject_id] -m
```

Example:

```
/group_shares/fnl/bulk/projects/RODENT/CYA/processed/testrun/OptoRat_M209_20180613/20180613$ /group_shares/fnl/bulk/projects/RODENT/Rat_misc/Rat_pull_data/run_rodent_subj.sh -o /group_shares/fnl/bulk/projects/RODENT/CYA/processed/testrun/OptoRat_M209_20180613/20180613 -s OptoRat_M209_20180613 -m
```
    
    - Output: `topup_applied_chpixdim.nii.gz` `top_up_mask.nii.gz`


### Edit Topup Mask

1. Navigate to subject folder (Example: `/group_shares/fnl/bulk/projects/RODENT/CYA/processed/Rat_Opto_fMRI/OptoRat_M314_20180420/20180420/`)
2. Select `topup_applied_chpixdim.nii.gz` & `top_up_mask.nii.gz`, open in FSLView
3. Edit topup mask
	- In the bottom right, unclick the lock on the mask file, click blue "i" icon, change "lookup table options" to Red. 
	- When done save as: `top_up_mask_EDIT.nii.gz` (Output: top_up_mask_EDIT.hdr & top_up_mask_EDIT.img)


## IV. Run Pipeline

1. Edit command for `run_rodent_subj.sh` with option `-p` to run subject through pipeline

Command:

```
/group_shares/fnl/bulk/projects/RODENT/Rat_misc/Rat_pull_data/run_rodent_subj.sh -o [output_directory] -s [subject_id] -p
```

Example:

```
/group_shares/fnl/bulk/projects/RODENT/CYA/processed/testrun/OptoRat_M209_20180613/20180613$ /group_shares/fnl/bulk/projects/RODENT/Rat_misc/Rat_pull_data/run_rodent_subj.sh -o /group_shares/fnl/bulk/projects/RODENT/CYA/processed/testrun/OptoRat_M209_20180613/20180613 -s OptoRat_M209_20180613 -p

```

- Once job complete check output file for last two lines 

```
FAIRPRE_FSL_13* completed successfully!
FAIRPRE_FSL_13* cleaned successfully.
```

- Check quality of the `xr3d_atl_res.4dfp.hdr` file under the 17n folder with FSLView (Example path: /group_shares/fnl/bulk/projects/RODENT/CYA/processed/Rat_Opto_fMRI/OptoRat_M314_20180420/20180420/FAIRPRE_FSL_13_RAT_TOPUP_LIN/REG_LIN_NODFM_3_FUNC1/17nT4IM_xr3d1)



## V. Run Seed Correl

Run Seed Correl
.../run_rodent_subj.sh -o [output_directory] -s [subject_id] -c


---

## DFM Preproc

Step 1:
Pull data
.../run_rodent_subj.sh -o [output_directory] -s [subject_id] -b [raw_bruker_data_path] -t [T2w_folder_number] -e [list_of_epi_folder_numbers_as_string] -d [dfm_folder_number]

**Output: b2n_data, b2n_rev_data, FWD_EPIs, DFM, MAG, MAG_mask, PHASE, T2_late2epi, T2_late2epi_d, T2_late2epi_d_bc, T2_mask_RBET_mask**

**Edit MAG_mask and save as MAG_mask_EDIT**

Step 2:
Run Correction Step on MAG_mask_EDIT
.../run_rodent_subj.sh -o [output_directory] -s [subject_id] -d [dfm_folder_number]

**Note: Uses the same option to run correction step**

**Output: uphase_rad**

Step 3:
Run DFM Pipeline
.../run_rodent_subj.sh -o [output_directory] -s [subject_id] -f

Step 4:
Run Seed Correl for DFM
.../run_rodent_subj.sh -o [output_directory] -s [subject_id] -f -c

---


## FEAT Analysis

*Needs to be updated with help from Marty Pike and Eric Earl*




