# Feat Analysis

[FEAT](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FEAT) is a tool for model-based fMRI data analysis. Modelling is based on general linear modelling (GLM), multiple regression. A model is created to fit the experiment design, labeling where the brain has activated in response to the stimuli. For additional information view the [FEAT User Guide](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FEAT/UserGuide#Appendix_B:_Design_Matrix_Rules).

## Requirements

Before running Feat analysis the following need to be completed:
- Scans need to be processed through the [opto-fMRI_pipeline](https://gitlab.com/Fair_lab/RODENT/blob/master/opto-fMRI/opto_feat/opto_feat_processing.md)
- [Timing files](https://gitlab.com/Fair_lab/RODENT/blob/master/opto-fMRI/opto_feat/timingfiles.md) need to be in the correct format


## First level analysis

First level analysis (time-series) data is completed with the FILM (FMRIB's Improved Linear Model) GLM method which is a nonparametric estimation of time series autocorrelation to prewhiten each voxel's time series.

### Quick Notes:

- Open `Feat`
- Load First-level analysis design.fsf 
- Select 4D data FWD_EPI#_final.nii.gz
- Select output folder name (ex: 3Hz_1mA or e1s12)
- Register to standard space using atlas
- Load timing file
- Click Go

### Detailed Notes:

** PICS: NOTE THAT DIFFERENT VERSIONS OF FSL MAKE THE CHECK BOXES LOOK DIFFERENT. Some are checked when selected, others are yellow when selected. **

1. Open Feat - in terminal type `Feat`
2. Data Tab 
    - Load design.fsf file - located in previous Feat analysis folders (example location: /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/output/PIKE04162019_MOUSE/20190416/feat_alina/1st_level/e1s12.feat/design.fsf)
    - Select 4D data - This is the final processed EPI (example location: /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/output/PIKE04162019_MOUSE/20190416/opto-fMRI_pipeline/FWD_EPI1_final.nii.gz)
    - Select output folder name - make a new directory in the subjects folder called feat & one within that called 1stlevel (example command: `mkdir feat_alina` & `mkdir 1st_level`
- ![](screenshots/pic_data.png)
3. Pre-stats Tab 
    - Motion correction: None
    - Slice timing correction: Interleaved (0, 2, 4 ... 1, 3, 5 ...)
    - Highpass: on
    - MELODIC ICA data exploration: on
- ![](screenshots/pic_pre-stats.png)
4. Registration Tab (Important! if you dont run this you wont be able to run higher level analysis)
    - Register to Standard space. Select the correct atlas for mice or rats
    - example mouse atlas /home/exacloud/tempwork/fnl_lab/atlases/mouse/c57_11/c57_11_M_Normal_age12_200200200
    - example rat atlas /home/exacloud/tempwork/fnl_lab/atlases/rat/EVA_TRANSFORM/ATLAS/EVA_TRANSFORM.nii.gz
- ![](screenshots/pic_registration.png)
5. Stats Tab
    - Use FILM prewhitening
    - Full model setup - Basic shape: Custom (3 column format)
    - Select timing file (example location: /home/exacloud/tempwork/fnl_lab/data/rodent/timing_files/PIKE04162019_MOUSE/12.txt)
    - Need "other" read (chmod o+r) privileges on timing file so FEAT can use
- ![](screenshots/pic_stats.png)
- ![](screenshots/pic_glm.png)
6. Misc Tab 
    - Estimate High Pass Filter
- ![](screenshots/pic_misc.png)
7. Post-Stats Tab
    - Cluster Thresholding - z = 2.3, p = 0.05
- ![](screenshots/pic_post-stats.png)
8. Click Go
9. Review Report & Data
    - To review report, open `report.html` (example command: `firefox feat_alina/1st_level/e1s12.feat/report.html`)
    - To review files open the `tstat1.nii.gz` & `filtered_func_data.nii.gz` (example command: fslview_deprecated feat_alina/1st_level/e1s12.feat/stats/tstat1.nii.gz )
        - In fslview, set the tstat1 min & max threshold appropriately (example: min 1.5, max 2)



## Higher level analysis

Higher level analysis is done acrosss sessions or subjects. FLAME (FMRIB's Local Analysis of Mized Effects) is used to model and estimate the random-effects component of the measured inter-session mied-effects variance, using MCMC sampling for estimation of the true random-effects variance and degrees of freedon at each voxel.

### Quick Notes:

- Open `Feat`
- Load Higher-level analysis design.fsf 
- Edit Number of inputs
- Select FEAT directories 
- Select output folder name (ex: 3Hz_1mA or e1-4s12-15)
- Click Go


### Detailed Notes:

1. Replace every first-level reg folder's `example_func2standard.mat` with the corresponding opto-fMRI_pipeline EPI# to atlas mat file
    - Copy the subjects FWD_EPI#_final_to_ATLAS.mat into the feat reg folder. Then rename that to `example_func2standard.mat`
    - Do this for each of the EPIs
    - Example commands 
```
cp opto-fMRI_pipeline/FWD_EPI1_final_to_ATLAS.mat feat_thomas/1st_level/e1s12.feat/reg/example_func2standard.mat
cp opto-fMRI_pipeline/FWD_EPI2_final_to_ATLAS.mat feat_thomas/1st_level/e2s13.feat/reg/example_func2standard.mat
cp opto-fMRI_pipeline/FWD_EPI3_final_to_ATLAS.mat feat_thomas/1st_level/e3s14.feat/reg/example_func2standard.mat
cp opto-fMRI_pipeline/FWD_EPI4_final_to_ATLAS.mat feat_thomas/1st_level/e4s15.feat/reg/example_func2standard.mat
```
2. Open Feat - in terminal type `Feat`
3. Data Tab 
    - Load design.fsf file - located in previous Feat analysis folders (example location: /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/output/PIKE03292019_MOUSE/20190329/feat_alina/2nd_level/e1-11s10-20.gfeat/design.fsf)
    - Changer the Number of inputs & Select FEAT directories - These are the first-level feat folders (example location: /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/output/PIKE04162019_MOUSE/20190416/feat_thomas/1st_level/e1s12.feat)
    - Select output folder name - make a new directory in the subjects feat folder called feat 2ndlevel (example command: `mkdir 2nd_level`
- ![](screenshots/pic_2data.png)
4. Stats Tab
    - Mixed effects: FLAME1
    - Full model setup - Make sure all EV1s are at 1.0
- ![](screenshots/pic_2stats.png)
- ![](screenshots/pic_2glm.png)
5. Misc Tab 
- ![](screenshots/pic_2misc.png)
6. Post-Stats Tab
    - Cluster Thresholding - z = 2.3, p = 0.05
- ![](screenshots/pic_2post-stats.png)
9. Click Go
10. Review Report & Data
    - To review report, open `report.html` (example command: `firefox feat_thomas/2nd_level/e1-4s12-15.gfeat/report.html`)
    - To review files open the `zstat1.nii.gz` & `filtered_func_data.nii.gz` & `bg_image.nii.gz` (example command: fslview_deprecated feat_thomas/2nd_level/e1-4s12-15+.gfeat/bg_image.nii.gz feat_thomas/2nd_level/e1-4s12-15+.gfeat/cope1.feat/zstat1.nii.gz feat_thomas/2nd_level/e1-4s12-15+.gfeat/mean_func.nii.gz )
        - In fslview, set the tstat1 min & max threshold appropriately (example: min 1.5, max 2)


