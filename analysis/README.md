# Analysis

- [feat.md](feat.md) Model-based (GLM multiple regression) opto-fMRI data analysis via feat
- [melodic_ica.md](melodic_ica.md) Independent component analysis decomposing data sets into different spatial and temporal components
- [volumeanalysis.md](volumeanalysis.md) Volume analysis of structural MRI
- For opto-fMRI data analysis via SPM code from Pittsburgh, go to [spm_processing.md](../image_processing/spm_processing.md) fMRI 