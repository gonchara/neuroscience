# Volume Analysis

The current scripts used are set up for volume analysis on the Rat IL6 cohort which has two time points. Scripts can be modified to work with other cohorts.

scripts:
- [ANTS_ROI_VOLUMES_wrapper.sh](scripts/ANTS_ROI_VOLUMES_wrapper.sh)
- [ANTS_ROI_VOLUMES.bash](scripts/ANTS_ROI_VOLUMES.bash)

---

- To run this analysis on a list of subjects, edit the wrapper `ANTS_ROI_VOLUMES_wrapper.sh`
- Paste path to the wrapper in a terminal & run

---

- To run this script on one subject, paste the path to it in a terminal followed by the path to the subject's data. You will need to scroll down to "NAME SUBJ & WAVE" to change the argument variable for subj.  

Example: 
```
/mnt/max/shared/projects/RODENT/scripts/ANTS_ROI_VOLUMES.bash /mnt/max/shared/aircreadwrite/CYA/processed/Rat_IL6_AFTER_WBR/IL5_1MC_W2/20170612
```

This script registers the study template for each wave (i.e. age group), to each subject.
Because the EVA & SHWARZ atlases are in the same space as the template, 
the transforms from the template registration can be used to transform 
both roi sets to each subject.
- Once registered, the volumes are extracted and pasted into a txt file.
- If you are rerunning this, and the previous vol folder exists, scroll down 
	to uncomment the section that either removes or renames the previous vol folder

