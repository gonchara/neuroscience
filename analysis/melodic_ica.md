# Melodic ICA

[MELODIC](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/MELODIC) ( Multivariate Exploratory Linear Optimized Decomposition into Independent Components ) uses ICA (Independent Component Analysis) allows exploration of the different spatial and temporal components of the data. This can help see ghosting or motion artifacts that can be removed to clean the data.

## Melodic ICA via command line

### Run Melodic ICA

1. Create directory for ica outputs (Example command: `mkdir /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/output/PIKE04162019_MOUSE/20190416/ica`)
2. Edit command & run

`melodic -i /$EPI_path -o $output_name -v --nobet --bgthreshold=3 --tr=1.000000 --report --guireport=../../report.html -d 0 --mmthresh=0.5 --Ostats`

Example Commands:
```
melodic -i /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/output/PIKE04162019_MOUSE/20190416/FWD_EPI1.nii.gz -o EPI1 -v --nobet --bgthreshold=3 --tr=1.000000 --report --guireport=../../report.html -d 0 --mmthresh=0.5 --Ostats
melodic -i /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/output/PIKE04162019_MOUSE/20190416/FWD_EPI2.nii.gz -o EPI2 -v --nobet --bgthreshold=3 --tr=1.000000 --report --guireport=../../report.html -d 0 --mmthresh=0.5 --Ostats
```

```
srun --mem 5G -t 1:00:00 /home/exacloud/tempwork/fnl_lab/code/external/utilities/fsl-5.0.6-HCP/bin/melodic -i /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/output/PIKE02112019_RAT4303_ELEC_STIM/20190211/FWD_EPI3.nii.gz -o EPI3 -v --nobet --bgthreshold=3 --tr=1.000000 --report --guireport=../../report.html -d 0 --mmthresh=0.5 --Ostats
```

![ica1.png](screenshots/ica1.png)
![ica2.png](screenshots/ica2.png)

### Review Report & Remove Components

1. Open report `00index.html` (Example command: firefox EPI1/report/00index.html)
2. Pick out artifact componenents
    - View individual components

Example command:

```
fslview_deprecated /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/output/PIKE02112019_RAT4303_ELEC_STIM/20190211/ica/EPI2/stats/thresh_zstat5.nii.gz
```

3. Remove artifact components
    - Edit command `fsl_regfilt -i /$EPI_path.nii.gz -d /$melodicdata_path/$ica/$EPI1/melodic_mix -o /$output_name/$ica/$EPI1_ica_rm_2_3_6_7_13_19.nii.gz -f "$component numbers$ 2,3,6,7,13,19"

Example commands:
```
fsl_regfilt -i /mnt/max/shared/data/rodent/opto-fmri/output/PIKE02112019_RAT4303_ELEC_STIM/20190211/FWD_EPI1.nii.gz -d /mnt/max/shared/data/rodent/opto-fmri/output/PIKE02112019_RAT4303_ELEC_STIM/20190211/ica/EPI1/melodic_mix -o /mnt/max/shared/data/rodent/opto-fmri/output/PIKE02112019_RAT4303_ELEC_STIM/20190211/ica/EPI1_ica_rm_1-10_12-14_19_22-23.nii.gz -f "1,2,3,4,5,6,7,8,9,10,12,13,14,19,22,23"
```

- View difference between original nii and cleaned nii (Example: fslview_deprecated $name.nii.gz)


## Run via GUI

- Open Melodic GUI with `Melodic` command
- Select 4D Data (Example: /home/exacloud/tempwork/fnl_lab/data/rodent/opto-fmri/output/PIKE02112019_RAT4303_ELEC_STIM/20190211/FWD_EPI1.nii.gz)
- Select output folder name 
- Go

![icagui.png](screenshots/icagui.png)
---
