#!/bin/bash 
set -o errexit
set -o pipefail
set -o nounset
set -o xtrace #Uncomment for debugging


################################################################################
# ABOUT THIS SCRIPT (Questions? Contact Alina Goncharova)
# - To run this script on a list of subjects, edit the wrapper /mnt/max/shared/projects/RODENT/scripts/ANTS_ROI_VOLUMES_wrapper.sh
#		Then past the path to the wrapper in a terminal. 
# - To run this script on one subject, paste the path to it in a terminal followed
# 		by the path to the subject's data. You will need to scroll down to "NAME SUBJ & WAVE"
#		to change the argument variable for subj.  Example: 
#		/mnt/max/shared/projects/RODENT/scripts/ANTS_ROI_VOLUMES.bash /mnt/max/shared/aircreadwrite/CYA/processed/Rat_IL6_AFTER_WBR/IL5_1MC_W2/20170612


# - This script registers the study template for each wave (i.e. age group), to each subject.
# - Because the EVA & SHWARZ atlases are in the same space as the template, 
#   	the transforms from the template registration can be used to transform 
#		both roi sets to each subject.
# - Once registered, the volumes are extracted and pasted into a txt file.
# - If you are rerunning this, and the previous vol folder exists, scroll down 
#		to uncomment the section that either removes or renames the previous vol folder

################################################################################


#################### SET PATH VARIABLES ########################################

W1template="/mnt/max/shared/ROI_sets/Surface_schemes/Rat/ILSP_W1_template/IL6_brain_template_W1.nii.gz"
W1EVArois="/mnt/max/shared/ROI_sets/Surface_schemes/Rat/ILSP_W1_template/multilabel_EVA_ROIs2StudyTemplate.nii.gz"
W1SHWARZrois="/mnt/max/shared/ROI_sets/Surface_schemes/Rat/ILSP_W1_template/multilabel_SHWARZ_ROIs2StudyW1Template.nii.gz"

W2template="/mnt/max/shared/ROI_sets/Surface_schemes/Rat/ILSP_W2_template/IL6_brain_template_W2.nii.gz"
W2EVArois="/mnt/max/shared/ROI_sets/Surface_schemes/Rat/ILSP_W2_template/multilabel_EVA_ROIs2StudyTemplate.nii.gz"
W2SHWARZrois="/mnt/max/shared/ROI_sets/Surface_schemes/Rat/ILSP_W2_template/multilabel_SHWARZ_ROIs2StudyW2Template.nii.gz"

scripts="/mnt/max/shared/projects/RODENT/scripts"
analyses="/mnt/max/shared/aircreadwrite/CYA/analyses_v2/volume/Rat_IL6"

#################### CREATE FUNCTIONS ##########################################
# This creates the functions, but does not call to them yet.  

## REGISTER TEMPLATE TO SUBJECT SPACE ##
function register_template() {
   subj_folder="${1}" # 1st func arg
   template="${2}" # 2nd func arg
   #rois="${3}" # 3rd func arg

	#antsRegistrationSyN.sh -d dimentionality -f fixed img (subj T2) -m moving img (template) -o output prefix
	if [ ! -f "${subj_folder}"/vol/StudyTemplate2Subj_1Warp.nii.gz ]; then
		"${scripts}"/antsRegistrationSyN.sh -d 3 -f "${subj_folder}"/vol/maskedT2_dbc.nii.gz -m "${template}" -o "${subj_folder}"/vol/StudyTemplate2Subj_	
	fi
}

## TRANSFORM EVA ROIS TO SUBJECT SPACE ##
function transform_EVA() {
   subj_folder="${1}" # 1st func arg
   template="${2}" # 2nd func arg
   rois="${3}" # 3rd func arg

	# antsApplyTransforms -d dimensionality -i input moving img (ROIs) -r reference (subj T2) -t transforms (warp & affine.mat) -n interpolation (Multilabel) -o output
	if [ ! -f "${subj_folder}"/vol/EVA_ROIs2StudyTemplate2Subj.nii.gz ]; then
		antsApplyTransforms -d 3 -i "${rois}" -r "${subj_folder}"/vol/maskedT2_dbc.nii.gz -t "${subj_folder}"/vol/StudyTemplate2Subj_1Warp.nii.gz "${subj_folder}"/vol/StudyTemplate2Subj_0GenericAffine.mat -n MultiLabel -o "${subj_folder}"/vol/EVA_ROIs2StudyTemplate2Subj.nii.gz
	fi
}

## TRANSFORM SHWARZ ROIS TO SUBJECT SPACE ##
function transform_SHWARZ() {
   subj_folder="${1}" # 1st func arg
   template="${2}" # 2nd func arg
   rois="${3}" # 3rd func arg

	# antsApplyTransforms -d dimensionality -i input moving img (ROIs) -r reference (subj T2) -t transforms (warp & affine.mat) -n interpolation (Multilabel) -o output
	if [ ! -f "${subj_folder}"/vol/SHWARZ_ROIs2StudyTemplate2Subj.nii.gz ]; then
		antsApplyTransforms -d 3 -i "${rois}" -r "${subj_folder}"/vol/maskedT2_dbc.nii.gz -t "${subj_folder}"/vol/StudyTemplate2Subj_1Warp.nii.gz "${subj_folder}"/vol/StudyTemplate2Subj_0GenericAffine.mat -n MultiLabel -o "${subj_folder}"/vol/SHWARZ_ROIs2StudyTemplate2Subj.nii.gz
	fi	
}

####################  NAME SUBJ & WAVE ###############################################

subj=${1} # IF RUNNING ONE SUBJECT CHANGE THIS to ${2} 
subj_vol="${subj}/vol"
echo "$subj"
echo "${subj_vol}=${subj}/vol"

#This name subjects as W1 or W2 
	if [[ $subj == *"W1"* ]]; then
  		echo "It's W1"
  		Wave1=1;
  		Wave2=0;
	fi
 
	if [[ $subj == *"W2"* ]]; then
  		echo "It's W2"
  		Wave2=1;
  		Wave1=0;
	fi

#################### SET UP FOLDERS ############################################

# RERUNNING? REMOVE OR RENAME PREVIOUS VOL FOLDER #
# - To remove previous vol directory uncomment the section below. !!!USE CAREFULLY!!!
#	if [ -d "${subj}"/vol ]; then 
#		rm -rf "${subj}"/vol  
#	fi

# - To rename previous vol folder, uncomment the 3 lines below
	if [ -d "${subj}"/vol ]; then 
		mv "${subj}"/vol "${subj}"/wrong_vol 
	fi

#Set up folders
	if [ ! -d "${subj}"/vol ]; then
		mkdir "${subj}"/vol
		mkdir "${subj}"/vol/T2_volumes
		#mkdir "${subj}"/vol/T2_volumes/singleROIs
		cp "${subj}"/T2_late2epi_dn_bc.nii.gz "${subj}"/vol/T2_late2epi_d_bc.nii.gz
		cp "${subj}"/T2_mask_RBET_mask_EDIT.hdr "${subj}"/vol/T2_mask_RBET_mask_EDIT.hdr
		cp "${subj}"/T2_mask_RBET_mask_EDIT.img "${subj}"/vol/T2_mask_RBET_mask_EDIT.img
	fi


	if [ ! -f "${subj_vol}"/maskedT2_dbc.nii.gz ]; then
   		fslmaths "${subj_vol}"/T2_late2epi_d_bc.nii.gz -mas "${subj_vol}"/T2_mask_RBET_mask_EDIT.hdr "${subj_vol}"/maskedT2_dbc.nii.gz
	fi


#################### CALL FUNCS ####################
# This section calls the function that was created above, while allowing the subjects, templates and roi sets to be different. 
#launch the appropriate function for W1 or W2
#study=$(dirname "$(dirname "${subj}")") 

## REGISTER TEMPLATE TO SUBJECT - GET WARP + TRANSFORM
	if [ ! -e "${subj}"/vol/StudyTemplate2Subj_1Warp.nii.gz ]; then
		if [ "$Wave1" -eq "1" ]; then
			register_template "${subj}" "${W1template}" 
		fi
		if [ "$Wave2" -eq "1" ]; then
			register_template "${subj}" "${W2template}" 
		fi
	fi
    wait

## TRANSFORM EVA & SHWARZ ROIS TO SUBJ SPACE
	if [ ! -e "${subj}"/vol/EVA_ROIs2StudyTemplate2Subj.nii.gz ]; then
		if [ "$Wave1" -eq "1" ]; then
			transform_EVA "${subj}" "${W1template}" "${W1EVArois}" 
			transform_SHWARZ "${subj}" "${W1template}" "${W1SHWARZrois}" 
		fi
		if [ "$Wave2" -eq "1" ]; then
			transform_EVA "${subj}" "${W2template}" "${W2EVArois}" 
			transform_SHWARZ "${subj}" "${W2template}" "${W2SHWARZrois}" 
		fi
	fi
    wait

#################### COPY FILES ####################

	if [ ! -f "${subj_vol}"/T2_volumes/EVA_ROIs2StudyTemplate2Subj.nii.gz ]; then
		cp "${subj_vol}"/EVA_ROIs2StudyTemplate2Subj.nii.gz "${subj_vol}"/T2_volumes/EVA_ROIs2StudyTemplate2Subj.nii.gz
		cp "${subj_vol}"/maskedT2_dbc.nii.gz "${subj_vol}"/T2_volumes/maskedT2_dbc.nii.gz
	fi

	if [ ! -f "${subj_vol}"/T2_volumes/SHWARZ_ROIs2StudyTemplate2Subj.nii.gz ]; then
		cp "${subj_vol}"/SHWARZ_ROIs2StudyTemplate2Subj.nii.gz "${subj_vol}"/T2_volumes/SHWARZ_ROIs2StudyTemplate2Subj.nii.gz
	fi

## FOR RERUNNING SHWARZ TO CORRECT 10/17/18
#	if [ ! -f "${subj_vol}"/T2_volumes/fixedSHWARZ_ROIs2StudyTemplate2Subj.nii.gz ]; then
#	cp "${subj_vol}"/fixedSHWARZ_ROIs2StudyTemplate2Subj.nii.gz "${subj_vol}"/T2_volumes/fixedSHWARZ_ROIs2StudyTemplate2Subj.nii.gz
#	fi

#################### EXTRACT VOLUME DATA INTO TXT ####################

subj_t2=${subj}/vol/T2_volumes
	if [ ! -f "${subj_t2}"/EVA_scan_volumes.txt ]; then
		c3d "${subj_t2}"/maskedT2_dbc.nii.gz "${subj_t2}"/EVA_ROIs2StudyTemplate2Subj.nii.gz -lstat > "${subj_t2}"/EVA_scan_volumes.txt
	fi

	if [ ! -f "${subj_t2}"/Shwarz_scan_volumes.txt ]; then
		c3d "${subj_t2}"/maskedT2_dbc.nii.gz "${subj_t2}"/SHWARZ_ROIs2StudyTemplate2Subj.nii.gz -lstat > "${subj_t2}"/Shwarz_scan_volumes.txt
	fi

## FOR RERUNNING SHWARZ TO CORRECT 10/17/18
#	if [ ! -f "${subj_t2}"/fixedShwarz_scan_volumes.txt ]; then
#	c3d "${subj_t2}"/maskedT2_dbc.nii.gz "${subj_t2}"/fixedSHWARZ_ROIs2StudyTemplate2Subj.nii.gz -lstat > "${subj_t2}"/fixedShwarz_scan_volumes.txt
#	fi

#################### RENAME TXT TO SUBJECT NAME & MOVE TO ANALYSIS FOLDER ####################

subject_name=$(basename "$(dirname "${subj}")")  #(dirname "$(dirname "/mnt/max/shared/aircreadwrite/Staff/Alina/VolumeAnalyses/IL2_1MC_W1/06242016")")
subj_t2=${subj}/vol/T2_volumes

	if [ -f "${subj_t2}"/EVA_scan_volumes.txt ]; then
		cp "${subj_t2}"/EVA_scan_volumes.txt "${analyses}"/EVA/"${subject_name}"_EVA_scan_volumes.txt
	fi

	if [ -f "${subj_t2}"/Shwarz_scan_volumes.txt ]; then
		cp "${subj_t2}"/Shwarz_scan_volumes.txt "${analyses}"/SHWARZ/"${subject_name}"_Shwarz_scan_volumes.txt
	fi

# uncommented for rerunning shwarz on 10/17/18
#	if [ -f "${subj_t2}"/fixedShwarz_scan_volumes.txt ]; then
#	cp "${subj_t2}"/fixedShwarz_scan_volumes.txt "${analyses}"/SHWARZ/"${subject_name}"_fixedShwarz_scan_volumes.txt
#	fi


popd

exit