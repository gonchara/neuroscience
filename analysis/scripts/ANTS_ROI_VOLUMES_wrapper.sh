#!/bin/bash 
set -o errexit
set -o pipefail
set -o nounset
set -o xtrace #Uncomment for debugging

################################################################################
# ABOUT THIS SCRIPT (Questions? Contact Alina Goncharova)
# - To run this script update the path to your csv, then paste the path to this 
#		wrapper into a terminal. /mnt/max/shared/projects/RODENT/scripts/ANTS_ROI_VOLUMES_wrapper.sh


# - This script is a wrapper for /mnt/max/shared/projects/RODENT/scripts/ANTS_ROI_VOLUMES.bash
#	It allows you to loop through a list of subjects 
################################################################################


# - REQUIRES CSV - To create the list, run list generating command in terminal. Example:
# 		find /mnt/max/shared/aircreadwrite/CYA/processed/Rat_IL6_AFTER_WBR -maxdepth 2 -mindepth 2 -type d -print | xargs -I{} sh -c "echo {} >> List3.csv" | xargs -n2
# - Add the csv path to this line: for master in `cat /mnt/max/shared/aircreadwrite/Staff/Alina/VolumeAnalyses/List.csv`;do

# STUDY DATA csv: /mnt/max/shared/aircreadwrite/CYA/processed/Rat_IL6_AFTER_WBR/List.csv 
# TESTING DATA csv: /mnt/max/shared/aircreadwrite/Staff/Alina/VolumeAnalyses/List.csv


IFS=$'\n'
for master in `cat /mnt/max/shared/aircreadwrite/CYA/processed/Rat_IL6_AFTER_WBR/List.csv`;do #ADD PATH TO CSV HERE
subj=${master%,*}
subj_vol="${master%,*}/vol"
echo "$subj"
echo "${subj_vol}=${subj}/vol"

/mnt/max/shared/projects/RODENT/scripts/ANTS_ROI_VOLUMES.bash "$subj" &
			
	runningJobs=$(jobs | wc -l | xargs)
  			if [ "$runningJobs" -ge 8 ]; then
    			wait
  			fi
done


